# 流程控制语句

在一个程序执行的过程中，各条语句的执行顺序对程序的结果是有直接影响的。所以，我们必须清楚每条语句的执行流程。而且，很多时候我们要通过控制语句的执行顺序来实现我们要完成的功能。

## 流程控制语句分类

-   顺序结构：按照代码的先后顺序，依次执行。结构图如下：

![if.png](https://i.loli.net/2020/05/15/i6VAMhP3p8XSkfY.png)

-   选择结构：if 语句、switch 语句

-   循环结构：while 语句、for 语句

## if 语句

if 语句有以下三种。

### 条件判断语句

> 条件成立才执行。如果条件不成立，那就什么都不做。

格式：

```javascript
if (条件表达式) {
	// 条件为真时，做的事情
}
```

### 条件分支语句

格式 1：

```javascript
if (条件表达式) {
	// 条件为真时，做的事情
} else {
	// 条件为假时，做的事情
}
```

格式 2：（多分支的 if 语句）

```javascript
if (条件表达式1) {
	// 条件1为真时，做的事情
} else if (条件表达式2) {
	// 条件1不满足，条件2满足时，做的事情
} else if (条件表达式3) {
	// 条件1、2不满足，条件3满足时，做的事情
} else {
	// 条件1、2、3都不满足时，做的事情
}
```

以上所有的语句体中，只执行其中一个。

```
根据BMI（身体质量指数）显示一个人的体型。
BMI指数，就是体重、身高的一个计算公式。公式是：
BMI =体重÷身高的平方

比如，老师的体重是81.6公斤，身高是1.71米。
那么老师的BMI就是  81.6 ÷ 1.712     等于 27.906022365856163

过轻：低于18.5
正常：18.5-24.99999999
过重：25-27.9999999
肥胖：28-32
非常肥胖, 高于32

用JavaScript开发一个程序，让用户先输入自己的体重，然后输入自己的身高（弹出两次prompt框）。
计算它的BMI，根据上表，弹出用户的身体情况。比如“过轻” 、 “正常” 、“过重” 、 “肥胖” 、“非常肥胖”。
```

写法 1：

```javascript
//第一步，输入身高和体重
var height = parseFloat(prompt("请输入身高，单位是米"));
var weight = parseFloat(prompt("请输入体重，单位是公斤"));
//第二步，计算BMI指数
var BMI = weight / Math.pow(height, 2);
//第三步，if语句来判断。注意跳楼现象
if (BMI < 18.5) {
	alert("偏瘦");
} else if (BMI < 25) {
	alert("正常");
} else if (BMI < 28) {
	alert("过重");
} else if (BMI <= 32) {
	alert("肥胖");
} else {
	alert("非常肥胖");
}
```

写法 2：

```javascript
//第一步，输入身高和体重
var height = parseFloat(prompt("请输入身高，单位是米"));
var weight = parseFloat(prompt("请输入体重，单位是公斤"));
//第二步，计算BMI指数
var BMI = weight / Math.pow(height, 2);
//第三步，if语句来判断。注意跳楼现象
if (BMI > 32) {
	alert("非常肥胖");
} else if (BMI >= 28) {
	alert("肥胖");
} else if (BMI >= 25) {
	alert("过重");
} else if (BMI >= 18.5) {
	alert("正常");
} else {
	alert("偏瘦");
}
```

### if 语句的跳楼现象

```
用户输入成绩，
如果成绩大于等于85，那么提示优秀；
否则如果成绩大于等于70，那么提示良好；
否则如果成绩60~69，那么提示及格；
否则，不及格

```

![cj.png](https://i.loli.net/2020/05/15/W3dxAwXeLUfay5Y.png)
![tl1.png](https://i.loli.net/2020/05/15/MtQdVmr1FgnvRSi.png)
![tl2.png](https://i.loli.net/2020/05/15/S3zo9sUAWE2FkmG.png)

### if 语句的嵌套

我们通过下面这个例子来引出 if 语句的嵌套。

```
一个加油站为了鼓励车主多加油，所以加的多有优惠。
92号汽油，每升6元；如果大于等于20升，那么每升5.9；
97号汽油，每升7元；如果大于等于30升，那么每升6.95
编写JS程序，用户输入自己的汽油编号，然后输入自己加多少升，弹出价格。
```

![jiayou.png](https://i.loli.net/2020/05/15/XuaNxr1Mn4UzJRf.png)

代码实现如下：

```javascript
//第一步，输入
var bianhao = parseInt(prompt("您想加什么油？填写92或者97"));
var sheng = parseFloat(prompt("您想加多少升？"));

//第二步，判断
if (bianhao == 92) {
	//编号是92的时候做的事情
	if (sheng >= 20) {
		var price = sheng * 5.9;
	} else {
		var price = sheng * 6;
	}
} else if (bianhao == 97) {
	//编号是97的时候做的事情
	if (sheng >= 30) {
		var price = sheng * 6.95;
	} else {
		var price = sheng * 7;
	}
} else {
	alert("对不起，没有这个编号的汽油！");
}

alert("价格是" + price);
```

## 三元运算符

```
语法
表达式?如果表达式结果为 true 执行这里的代码:如果表达式结果为 false 执行冒号后面的代码;
```

```javascript
5 > 3 ? "5大于3" : "5不大于3";
// 返回'5大于3'
var count = 30;
var price = count > 20 ? 6 : 7;
console.log(price);

// 可以理解为 if else 的另一种写法
```

## 代码调试

-   首先将程序执行完成后，点击 f12

-   设置断点

![ts.png](https://i.loli.net/2020/05/15/diMsK5CnulmkwLH.png)

-   运行程序

    -   刷新页面

-   程序一步一步执行 - 通过 f10 快键键或者点击

![ts1.png](https://i.loli.net/2020/05/15/xCa4uioK8ceSMkg.png)

-   监视变量变化 - 直接将鼠标放到变量名上即可显示 - 方式二通过 watch 窗口监事 - 直接选择变量名，点击鼠标右键选择 add watch 添加监事

![ts2.png](https://i.loli.net/2020/05/15/iKPq4VSnkF5jrcH.png)
![ts3.png](https://i.loli.net/2020/05/15/CIzeEFoUy1wZjM7.png)

## switch 语句（条件分支语句）

switch 语句也叫条件分支语句。

格式：

```javascript
switch(表达式) {
    case 值1：
            语句体1;
        break;

    case 值2：
        语句体2;
        break;

    ...
    ...

    default：
        语句体 n+1;
        break;
}
```

备注 1：当所有的比较结果都为 false 时，则只执行 default 里的语句。

备注 2：break 可以省略，但一般不建议。否则结果可能不是你想要的

### switch 语句的执行流程

-   首先，计算出表达式的值，和 case 依次比较，一旦有对应的值，就会执行相应的语句，在执行的过程中，遇到 break 就会结束。

-   然后，如果所有的 case 都和表达式的值不匹配，就会执行 default 语句体部分，然后程序结束掉。

### switch 语句的结束条件

-   情况 a：遇到 break 就结束，而不是遇到 default 就结束。（因为 break 在此处的作用就是退出 switch 语句）

-   情况 b：执行到程序的末尾就结束。

我们来举两个例子就明白了。

**举例 1**：

```javascript
var num = 4;

//switch判断语句
switch (num) {
	case 1:
		console.log("星期一");
		break;
	case 2:
		console.log("星期二");
		break;
	case 3:
		console.log("星期三");
		break;
	case 4:
		console.log("星期四");
	//break;
	case 5:
		console.log("星期五");
	//break;
	case 6:
		console.log("星期六");
		break;
	case 7:
		console.log("星期日");
		break;
	default:
		console.log("你输入的数据有误");
		break;
}
```

上方代码的运行结果，可能会令你感到意外：

```
星期四
星期五
星期六
```

上方代码的解释：因为在 case 4 和 case 5 中都没有 break，那语句走到 case 6 的 break 才会停止。

## for 循环

### for 循环的语法

语法：

```
for(初始化表达式; 条件表达式; 更新表达式){
    语句...
}
```

执行流程：

```
	执行初始化表达式，初始化变量（初始化表达式只会执行一次）

	执行条件表达式，判断是否执行循环：
		如果为true，则执行循环语句
		如果为false，终止循环

	执行更新表达式，更新表达式执行完毕继续重复
```

for 循环举例：

```javascript
for (var i = 1; i <= 100; i++) {
	console.log(i);
}
```

上方代码的解释：

![for.png](https://i.loli.net/2020/05/15/mSTtoOkZDQ39MLh.png)

### for 循环举例

```javascript
for (var i = 1; i < 13; i = i + 4) {
	console.log(i);
}
```

上方代码的遍历步骤：

```
程序一运行，将执行var i = 1;这条语句， 所以i的值是1。
然后程序会验证一下i < 13是否满足，1<13是真，所以执行一次循环体（就是大括号里面的语句）。
执行完循环体之后，会执行i=i+4这条语句，所以i的值，是5。

程序会会验证一下i < 13是否满足，5<13是真，所以执行一次循环体（就是大括号里面的语句）。
执行完循环体之后，会执行i=i+4这条语句，所以i的值，是9。

程序会会验证一下i < 13是否满足，9<13是真，所以执行一次循环体（就是大括号里面的语句）。
执行完循环体之后，会执行i=i+4这条语句，所以i的值，是13。

程序会会验证一下i < 13是否满足，13<13是假，所以不执行循环体了，将退出循环。

最终输出输出结果为：1、5、9
```

**题目 1**：

```javascript
for (var i = 1; i < 10; i = i + 3) {
	i = i + 1;
	console.log(i);
}
```

输出结果：2、6、10

**题目 2**：

```javascript
for (var i = 1; i <= 10; i++) {}
console.log(i);
```

输出结果：11

**题目 3**：

```javascript
for (var i = 1; i < 7; i = i + 3) {}
console.log(i);
```

输出结果：7

**题目 4**：

```javascript
for (var i = 1; i > 0; i++) {
	console.log(i);
}
```

死循环。

## while 循环语句

### while 循环

语法：

```javascript
while(条件表达式){
	语句...
}
```

执行流程：

```
while语句在执行时，先对条件表达式进行求值判断：

	如果值为true，则执行循环体：

		循环体执行完毕以后，继续对表达式进行判断
		如果为true，则继续执行循环体，以此类推

	如果值为false，则终止循环
```

**如果有必要的话，我们可以使用 break 来终止循环**。

### do...while 循环

语法：

```javascript
	do{
		语句...
	}while(条件表达式)

```

执行流程：

```
do...while语句在执行时，会先执行循环体：

循环体执行完毕以后，在对while后的条件表达式进行判断：
	如果结果为true，则继续执行循环体，执行完毕继续判断以此类推

	如果结果为false，则终止循环


```

### while 循环和 do...while 循环的区别

这两个语句的功能类似，不同的是：

-   while 是先判断后执行，而 do...while 是先执行后判断。

也就是说，do...while 可以保证循环体至少执行一次，而 while 不能。

### while 循环举例

**题目**：假如投资的年利率为 5%，试求从 1000 块增长到 5000 块，需要花费多少年？

**代码实现**：

```javascript
/*
 * 假如投资的年利率为5%，试求从1000块增长到5000块，需要花费多少年
 *
 * 1000 1000*1.05
 * 1050 1050*1.05
 */

//定义一个变量，表示当前的钱数
var money = 1000;

//定义一个计数器
var count = 0;

//定义一个while循环来计算每年的钱数
while (money < 5000) {
	money *= 1.05;

	//使count自增
	count++;
}

console.log(money);
console.log("一共需要" + count + "年");
```

打印结果：

```
	5003.18854203379

	一共需要33年
```

## break 和 continue

> 这个知识点非常重要。

### break

-   break 可以用来退出 switch 语句或**整个**循环语句（循环语句包括 for、while）。

-   break 会立即终止离它**最近**的那个循环语句。

**举例 1**：通过 break 终止循环语句

```javascript
for (var i = 0; i < 5; i++) {
	console.log("i的值:" + i);
	if (i == 2) {
		break; // 注意，虽然在 if 里 使用了 break，但这里的 break 是服务于外面的 for 循环。
	}
}
```

打印结果：

```
i的值:0
i的值:1
i的值:2
```

### continue

-   continue 可以用来跳过**当次**循环。

-   同样，continue 默认只会离他**最近**的循环起作用。
