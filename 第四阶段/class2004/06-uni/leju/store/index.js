import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    num:1,
  },
  mutations: {
    // commit 提交
    add(state,id){ // this.$store.commit("add",1)
    
     
      
    },
   upt(state){
	   state.num ++;
   }
  },
  // getters 类似于前面的computed
  getters:{
    total(state){ // this.$store.getters.total
      return state.aboutList.reduce((total,current)=>{
        if(current.isChecked){
          return total + current.price*current.num
        }else{
          return total
        }
        
      },0)
    },
    isAllChecked(state){ // true false
      if(state.aboutList.length == 0){
        return false
      }
      return state.aboutList.every(ele =>ele.isChecked)
    }
  },
  actions: {
    // actions  类似于前面的methods
    syncAdd(context,id){ // context 上下文对象 Store实例 this.$store.dispatch("syncAdd")  // dispatch 分发
    // id 载荷 一次只能传递一个参数  传递多个参数 需要转换成数组或对象的方式
      console.log(context.state.num);
      // 不推荐这种方式 数据的变化并没有反应到调试工具当中
      context.state.num ++ ;
      // setTimeout(()=>{
      //   context.commit("add",id)
      // },1000)
    }
  },
  // modules 模块
  modules: {
  }
})
