import Vue from 'vue'
import App from './App'
import http from "./utils/http.js"
Vue.config.productionTip = false
Vue.prototype.$http = http;
import store from "./store/index.js"
App.mpType = 'app'
import login from "./utils/isLogin.js"
// Vue.extend({
//   mixins: [login]
// })
// 在每一个组件实例上面 都会有混入的属性或方法
// Vue.mixin(login)
// Vue.prototype.$store = store;
const app = new Vue({
	
	store,
    ...App
})
app.$mount()
