import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'lib-flexible'

import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);

import VueAwesomeSwiper from 'vue-awesome-swiper'

// import style
// import 'swiper/css/swiper.css'
// If you use Swiper 6.0.0 or higher
import 'swiper/swiper-bundle.css'

Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false
import "@/assets/css/reset.css"
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
