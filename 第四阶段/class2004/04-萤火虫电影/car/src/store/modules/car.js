import mock from "mockjs"
var Random = mock.Random,arr=[];
for(var i = 0;i<10;i++){
    var num = Random.natural(0,10)
    var obj = {
        title:Random.ctitle(),
        id:Random.guid(),
        info:Random.cparagraph(),
        num:num,
        oldNum:num,
        price:Random.natural(10,100),
        img:Random.dataImage("200x100")
    }
    arr.push(obj)
}


var car = {
    namespaced: true,
    state: {
        arr,
        car:[]
    },
    mutations: {
        add(state,id){
            var item = state.arr.find(ele =>ele.id == id);
            var carItem = state.car.find(ele =>ele.id == id);
            if(carItem){
                carItem.num++
            }else{
                var obj = {
                    title:item.title,
                    id:item.id,
                    info:item.info,
                    num:1,
                    price:item.price,
                    img:item.img,
                    isChecked:false,
                    oldNum:item.oldNum
                }
                state.car.push(obj);
                
            }
            item.num --;
        },
        upt(state,val){
            state.car.forEach(ele =>{
                ele.isChecked = val
            })
        },
        uptEvents(state,{id,val}){
            var item = state.arr.find(ele =>ele.id == id);
            item.num = item.oldNum - val;
        }
    },
    actions: {
    
    },
    getters: {
        isAllChecked(state){
            if(state.car.length == 0){
                return false 
            }
            return state.car.every(ele =>ele.isChecked)
        },
        totals(state){
            return state.car.reduce((total,current)=>{
                if(current.isChecked){
                    return total + current.price*current.num
                }else{
                    return total
                }
            },0)
        }
    }
}

export default car
