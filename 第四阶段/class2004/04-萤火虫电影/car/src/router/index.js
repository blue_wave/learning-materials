import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { 
      isFootShow:true
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta: { 
      isFootShow:true
    }
  },
  {
    path: '/car',
    name: 'car',
    component: () => import('../views/car/index.vue'),
    meta: { 
      isFootShow:true
    }
  }
  ,
  {
    path: '/info/:id',
    name: 'info',
    component: () => import('../views/info/index.vue'),
    meta: { 
      isFootShow:false
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if(to.path == "/car"){
    if(localStorage.getItem("token")){
      next()
    }else{
      // next(false)
      alert("您没有登录，1秒之后跳转到登录页面")
      setTimeout(()=>{
        next("/about")
      },1000)
    }
  }else{
    next()
  }
  
})

export default router
