import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    num:1,
  },
  mutations: {
    inc(state,val){
      state.num += val
    },
    dec(state){
      state.num --
    }
  },
  getters:{
    age(state){
      return state.num *10 + "岁"
    }
  },
  actions: {
    syncInc(context){ // this.$store.dispatch("syncInc")
      // 在方法中执行的是提交mutations里面的方法 而不是直接修改对应的数据
      context.commit("dec")
    }
  },
  modules: {
  }
})
