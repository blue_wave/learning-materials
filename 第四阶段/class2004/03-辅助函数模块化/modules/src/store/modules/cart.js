// 进行购物车的操作
// 每个子模块都拥有自己的state mutations getters actions moduels
import mock from "mockjs";
var Random = mock.Random;
var list = []
for(var i = 0 ; i<5;i++){
    var obj = {
      name:Random.ctitle(),
      price:Random.natural(0,100),
      num:Random.natural(0,10),
      img:Random.dataImage("200x100"),
      id:Random.guid()
    }
    list.push(obj)
  }
var cart = {
    namespaced: true, // namespaced: true,  开启命名空间
    // 如果不开启命名空间  默认只要state里面的数据再访问的时候需要加上对应的模块名称 mutations/getters/actions 不需要加模块名称
    state:{
        num:1,
        list,
        carList:[]
    },
    mutations: {
        add(state,id){

            var item = state.list.find(ele =>ele.id == id);
            console.log(item);
            var obj = {
                id:item.id,
                name:item.name,
                price:item.price,
                num:1,
                isChecked:false
            };
            // if(item.num <= 0){
            //   return 
            // }
            // 添加之前先判断  aboutList 是否存在相同的商品
            var aboutItem  = state.carList.find(ele =>ele.id == id);
            if(aboutItem){
                aboutItem.num ++
            }else{
                state.carList.push(obj)
            }
            item.num -- 
        },
        dec(state,id){
            var carItem = state.carList.find(ele =>ele.id == id);
            var item = state.list.find(ele =>ele.id == id);
            if(carItem.num == 0){
                return 
            }
            carItem.num -- ;
            item.num ++

        }
    },
    getters:{
        totals(state){
            return state.carList.reduce((total,current)=>{
                return total+current.num * current.price
            },0)
        }
    },
    actions:{
        syncDec({commit},id){ // context = { commit }
            setTimeout(()=>{
                commit("dec",id)
            },1000)
        }
    }
}
export default cart