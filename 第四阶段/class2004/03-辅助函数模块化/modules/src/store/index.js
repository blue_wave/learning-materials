import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import car from "./modules/cart.js"
import user from "./modules/user.js"
export default new Vuex.Store({
  modules: {
    car,
    user
  }
})
