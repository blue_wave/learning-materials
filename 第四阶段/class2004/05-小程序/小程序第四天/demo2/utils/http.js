import baseUrl from "./url.js"
import qs from "./qs.js"
var obj = {
  get(url,params){
    return  new Promise((resolve,rejected)=>{
      wx.request({
        url: baseUrl + url,
        data:params,
        header:{
          "bufan-token":wx.getStorageSync('key')
        },
        success(res){
          resolve(res)
        }
      })
    })
  },
  post(url,params){
    return new Promise(function(resolve,rejected){
      wx.request({
        url: baseUrl+url,
        method:"POST",
        data:qs.stringify(params,{ allowDots: true}),
        header:{
          "content-type":"application/x-www-form-urlencoded",
          "bufan-token":wx.getStorageSync('key')
        },
        success(res){
          resolve(res)
        }
      })
    })
  }
}

export default obj