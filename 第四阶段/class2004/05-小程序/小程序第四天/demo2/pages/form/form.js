// pages/form/form.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    iptVal:"",
    rules: [
      {
          name: 'qq',
          // rules: {required: true, message: '输入框不能为空'},
          rules:[{required: true, message: '输入框不能为空'}, {mobile: true, message: 'mobile格式不对'}]
      }
    ],
    formData:{

    }
  },
  formInputChange(e) {
    // 获取定义在组件上面的自定义属性
    // 把获取到的key qq 赋值给formData   并把输入框的值赋值给qq
    const {field1} = e.currentTarget.dataset

    this.setData({
        [`formData.${field1}`]: e.detail.value
    })
    
},
submitForm(){
  this.selectComponent('#form1').validate((valid, errors) => {
      console.log('valid', valid, errors);
      console.log(this.data.formData)
      // if (!valid) {
      //     const firstError = Object.keys(errors)
      //     if (firstError.length) {
      //         this.setData({
      //             error: errors[firstError[0]].message
      //         })

      //     }
      // } else {
      //     wx.showToast({
      //         title: '校验通过'
      //     })
      // }
  })
},
  getIptVal(e){
    console.log(e);
    this.setData({
      iptVal:e.detail.value
    })
  },
  formSubmit(e){
   
    var iptVal = e.detail.value.username;
    console.log(e);
    wx.request({
      url: 'http://bufantec.com/api/test/user/checkUserName',
      method:"POST",
      data:{
        username:iptVal
      },
      header:{
        'content-type':"application/x-www-form-urlencoded"
      },
      success(res){
        console.log(res)
      }
    })
  },
  search(){
    wx.request({
      url: 'http://bufantec.com/api/test/user/checkUserName',
      method:"POST",
      data:{
        username:this.data.iptVal
      },
      header:{
        'content-type':"application/x-www-form-urlencoded"
      },
      success(res){
        console.log(res)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})