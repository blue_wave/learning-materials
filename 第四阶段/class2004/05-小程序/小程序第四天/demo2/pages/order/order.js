// pages/order/order.js
import http from "../../utils/http.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    error:"",
    rules: [{
      name: 'name',
      rules: [{
        required: true,
        message: '名称不能为空'
      }, {
        minlength: 2,
        message: '名称最小长度为2'
      }, {
        maxlength: 5,
        message: '名称最大长度为5'
      }],

    },
    {
      name: 'sex',
      rules: [{
        required: true,
        message: '性别必填'
      }],

    }],
    switch1Checked: true,
    formData: {

    },
    index: 0,
    array: [{
      name:"高中 ",
      id:0
    },
    {
      name:"大专 ",
      id:1
    },{
      name:"本科 ",
      id:2
    },
    {
      name:"本科以上 ",
      id:0
    },],
    items: [
      {value: 1, name: 'H5'},
      {value: 1, name: 'UI', checked: 'true'},
    ],
    date: '2016-09-01',
    region: ['广东省', '广州市', '海珠区'],
    customItem: '全部'
  },
  radioChange(e){
    const {
      field
    } = e.currentTarget.dataset;
    this.setData({
      [`formData.${field}`]: e.detail.value 
    })
  },
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    // this.setData({
    //   region: e.detail.value
    // })
  },
  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    const {
      field
    } = e.currentTarget.dataset;
    this.setData({
      date: e.detail.value,
      [`formData.${field}`]: e.detail.value
    })
  },
  switch1Change(e) {

    const {
      field
    } = e.currentTarget.dataset;
    this.setData({
      [`formData.${field}`]: e.detail.value == false ? "男" : "女"
    })
  },
  bindPickerChange(e){
    const {
      field
    } = e.currentTarget.dataset
    this.setData({
      index:e.detail.value,
      [`formData.${field}`]: e.detail.value
    })
  },
  // formInputChange  把数据及字段名称存放到 formData
  formInputChange(e) {
    const {
      field
    } = e.currentTarget.dataset
    this.setData({
      [`formData.${field}`]: e.detail.value
    })
  },
  submitForm() {
    this.selectComponent('#form').validate((valid, errors) => {
      console.log('valid', valid, errors)
      console.log(this.data.formData);
      if (!valid) {
          const firstError = Object.keys(errors)
          if (firstError.length) {
              this.setData({
                  error: errors[firstError[0]].message
              })

          }
      } else {
          var obj =  JSON.parse(JSON.stringify(this.data.formData));
          obj.id = wx.getStorageSync('token');
          var newVal = {
            xyUserLite:{
              ...obj
            }
          }
          http.post("/admin/xy/lite/student/orderZhan",newVal)
          .then(res =>{
            console.log(res)
          })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  getClassList(){
    http.get("/admin/xy/clazz/list")
    .then(res =>{console.log(res)})
  },
  login(){
    http.post("/admin/login/doLogin",{
      username:"admin",
      password:"123456"
    })
    .then(res =>{
      console.log(res);
      var key = res.data.token
      wx.setStorageSync('key', key);
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})