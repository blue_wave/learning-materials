// pages/login/login.js
import http from "../../utils/http";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:""
  },
  onLoad(){
    var token = wx.getStorageSync('token');
    if(token){
      this.setData({
        id:token
      })
    }
  },
  login(){
    var _this = this;
    wx.login({
      success (res) {
        if (res.code) {
          //发起网络请求
         http.get("/admin/xy/lite/student/doLogin",{
          code:res.code,
          appid:"wx5a6c58dfe9c70df6"
         })
         .then(res =>{
           console.log(res);
           if(res.data.code == true){
            _this.setData({
              id: res.data.data.id
            })
            wx.setStorageSync('token', res.data.data.id);
           }
         })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})