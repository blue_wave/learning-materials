import axios from "axios";

var hot = {
    namespaced: true,
    state: {
        list: [],
    },
    getters: {

    },
    mutations: {
        list(state, res) {
            state.list = res
        }
    },
    actions: {
        add(context) {
            axios.get('http://www.bufantec.com/api/douban/movie/in_theaters').then(res => {
                console.log(res.data.data.list);
                context.commit('list', res.data.data.list)
            }).catch(function (error) {
                console.log(error);
            })
        }
    },
}
export default hot