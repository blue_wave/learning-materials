import axios from "axios";

var video = {
    namespaced: true,
    state: {
        list: [],
    },
    getters: {

    },
    mutations: {
        list(state, res) {
            state.list = res
        }
    },
    actions: {
        add(context) {
            axios.get('http://www.bufantec.com/api/douban/movie/selectPreview').then(res => {
                console.log(res.data.data);
                context.commit('list', res.data.data)
            }).catch(function (error) {
                console.log(error);
            })
        }
    },
}
export default video