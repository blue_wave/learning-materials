import axios from "axios";

var movie = {
    namespaced: true,
    state: {
        show : true
    },
    getters: {

    },
    mutations: {
        show1(state) {
            state.show = false
        },
        fshow(state) {
            state.show = true
        },
    },
    actions: {

    },
}
export default movie