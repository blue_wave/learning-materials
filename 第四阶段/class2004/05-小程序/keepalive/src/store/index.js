import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import hot from './modules/hot.js'
import video from './modules/video.js'
import coming from './modules/coming.js'
import movie from './modules/movie.js'
export default new Vuex.Store({
  
  modules: {
    hot,
    coming,
    video,
    movie
  }
})
