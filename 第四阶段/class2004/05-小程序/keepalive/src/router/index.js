import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


const routes = [{
  path: '/',
  meta: {
    show: false
  },
  redirect: "/home"
},
{
  path: '/about',
  meta: {
    show: false
  },
  name: 'About',
  component: () => import( /* webpackChunkName: "about" */ '../views/About.vue')
},
{
  path: '/home',
  name: 'home',
  meta: {
    show: false,
    keepAlive:true
  },
  component: () => import('../views/home/index.vue')
},
{
  path: '/user',
  name: 'user',
  meta: { shwo: true,
    keepAlive:false
  },
  component: () => import('../views/user/index.vue')
},
{
  path: '/set',
  name: 'set',
  meta: { show: true },
  component: () => import('../views/user/set/set.vue')
},
{
  path: '/message',
  name: 'message',
  meta: { show: true },
  component: () => import('../views/user/message/message.vue')
},
{
  path: '/messageinfo',
  name: 'messageinfo',
  meta: { show: true },
  component: () => import('../views/user/message/info/messageinfo.vue')
},
{
  path: '/userinfo',
  name: 'userinfo',
  meta: { show: true },
  component: () => import('../views/user/message/info/userinfo.vue')
},
{
  path: '/card',
  name: 'card',
  meta: { show: true },
  component: () => import('../views/user/card/card.vue')
},
{
  path: '/user/:id',
  name: 'coupon',
  meta: { show: true },
  component: () => import('../views/user/card/coupon.vue')
},
{
  path: '/login',
  name: 'login',
  meta: { show: true },
  component: () => import('../views/user/login/login.vue')
},
{
  path: '/ticket',
  name: 'ticket',
  meta: { shwo: true },
  component: () => import('../views/ticket/index.vue')
},
{
  path: '/cinema',
  meta: {
    show: false
  },
  name: 'cinema',
  component: () => import('../views/cinema/index.vue')
},
{
  path: '/movie',
  meta: {
    show: false
  },
  name: 'Movie',
  component: () => import('../views/movie/index.vue'),

},
{
  path: '/sign',
  meta: {
    show: true
  },
  name: 'Sign',
  component: () => import('../views/sign/index.vue')
},
{
  path: '/details/:id',
  meta: {
    show: true
  },
  name: 'Details',
  component: () => import('../views/details/index.vue')
},

{
  path: '/user/orderList/:id',
  meta: {
    show: true
  },
  name: 'Details',
  component: () => import('../components/order/index.vue')
},

{
  path: '/buy/:id',
  meta: {
    show: true
  },
  name: 'Buy',
  component: () => import('../views/buy/index.vue')
},
{
  path: '/actor',
  meta: {
    show: true
  },
  name: 'Actor',
  component: () => import('../views/movie/Actor.vue')
},
{
  path: '/pingfen',
  meta: {
    show: true
  },
  name: 'Ping',
  component: () => import('../views/movie/Filmrating.vue')
},
{
  path: '/hotshow',
  meta: {
    show: false
  },
  name: 'Hotshow',
  component: () => import('../views/movie/Hotshow.vue')
},
{
  path: '/money',
  meta: {
    show: true
  },
  name: 'Money',
  component: () => import('../views/movie/piaofang.vue')
},
{
  path: '/adress',
  meta: {
    show: true
  },
  name: 'Adress',
  component: () => import('../views/movie/Posit.vue')
},
{
  path: '/movietalk',
  meta: {
    show: true
  },
  name: 'Talk',
  component: () => import('../views/movie/yingping.vue')
},
{
  path: '/movieadress',
  meta: {
    show: true
  },
  name: 'Movieadress',
  component: () => import('../views/movie/yingyuan.vue')
},
{
  path: '/movielist',
  meta: {
    show: true
  },
  name: 'Moviealist',
  component: () => import('../views/cinema/cinemaDetail/index.vue')
},
{
  path: '/myticket',
  meta: {
    show: true
  },
  name: 'Myticket',
  component: () => import('../views/cinema/myticket/index.vue')
},

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
