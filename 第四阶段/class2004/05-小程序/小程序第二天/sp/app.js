//app.js
// import qs from "./utils/qs.js";
import api from "./utils/api.js";

App({
  onLaunch: function () {
    // console.log(qs);

    // 先看本地是否有用户授权信息
    var userInfo = wx.getStorageSync("userInfo");

    if(!userInfo){
      // 查看鉴权
      wx.getSetting({
        success(res){
          // console.log(res);
          if (res.authSetting["scope.userInfo"]){
            wx.getUserInfo({
              success(res){
                // 直接设置在本地
                wx.setStorageSync("userInfo", res.userInfo)
              }
            })
          }
        }
      })
    }

    // 校验本地是否有用户id
    var uid = wx.getStorageSync("uid");
    if (!uid) {
      // 用户登陆
      wx.login({
        success(res) {
          // res.code 微信返回的用户登陆凭证,有效期 5分钟
          wx.request({
            url: api.login,
            data: {
              code: res.code,
              appid: "wxfe9fdca63ef12491" // 测试号appid
            },
            success(res) {
              console.log(res);
              // code 成功与否, data:{id: xxx} 用户id
              if (res.data.code) {
                wx.setStorageSync("uid", res.data.data.id);
              }
            }
          })
        }
      })
    }
  },
  globalData: {
  }
})