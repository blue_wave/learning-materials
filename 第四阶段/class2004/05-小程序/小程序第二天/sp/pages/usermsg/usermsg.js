// pages/usermsg/usermsg.js

import hs from "../../utils/httpserve.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    usermsg: null,
    education: ["高中", "专科", "本科", "本科以上"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // 获取个人信息
    hs.usermsg().then(res => {
      console.log(res);
      that.setData({
        usermsg: res.data
      })
    })
  },

  // 跳转到申请入学
  toStudy: function () {
    wx.navigateTo({
      url: '../applyStudy/applyStudy?sex='+this.data.usermsg.sex +"&classType="+this.data.usermsg.classType
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    // 获取个人信息
    hs.usermsg().then(res => {
      console.log(res);
      that.setData({
        usermsg: res.data
      })
      // 直接关闭下拉刷新
      wx.stopPullDownRefresh();
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})