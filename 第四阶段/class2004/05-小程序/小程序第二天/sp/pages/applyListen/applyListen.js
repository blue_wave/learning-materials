// pages/applyListen/applyListen.js

import api from "../../utils/api.js";
import hs from "../../utils/httpserve.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    sex: false,
    education: ["高中", "专科", "本科", "本科以上"],
    index: 0,
    haveJob: false,
    date: "",
    startDate: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var now = new Date().toLocaleDateString().replace(/\//g, "-");
    this.setData({
      date: now,
      startDate: now
    })
  },

  // 选择性别
  changeSex: function (e) {
    this.setData({
      sex: e.detail.value
    })
  },

  // 改变学历
  getEdu: function (e) {
    this.setData({
      index: e.detail.value
    })
  },

  // 是否有工作
  hasJob: function (e) {
    this.setData({
      haveJob: e.detail.value
    })
  },

  // 改变日期
  getDate: function (e) {
    this.setData({
      date: e.detail.value
    })
  },

  // 提交数据
  submit(e){
    // 表单的值
    // console.log(e.detail.value);
    // 将性别改成 number 类型
    e.detail.value.sex = +e.detail.value.sex;
    // 添加id
    e.detail.value.id = wx.getStorageSync('uid');
    hs.post({
      url: api.applyListen,
      data: e.detail.value,
      cb(){
        wx.navigateBack();
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})