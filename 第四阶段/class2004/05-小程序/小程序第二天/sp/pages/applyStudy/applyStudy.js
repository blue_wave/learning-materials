// pages/applyStudy/applyStudy.js

import api from "../../utils/api.js";
import hs from "../../utils/httpserve.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    cityText: ['北京市', '北京市', '东城区'],
    city: ["110000", "110100", "110101"],
    // 宿舍列表
    dormLs: [],
    // 选择宿舍的下标
    index: null,
    // 班级列表
    classLs: [],
    // 选择班级的下标
    count: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options);
    hs.getClass().then(res => {
      // console.log(res);
      var classLs = res.data.list;
      classLs = classLs.filter(item => {
        return item.type == options.classType;
      })

      that.setData({
        classLs: classLs
      })
    });
    hs.getDorm().then(res => {
      // console.log(res);
      var dormLs = res.data.list;
      dormLs = dormLs.filter(item => {
        return item.type == options.sex;
      })
      // 添加自选租住地选项
      // dormLs.push({ name: "自选租住地", id: null });
      dormLs.push({ name: "自选租住地" });

      that.setData({
        dormLs
      })
    });
  },

  // 选择原住地的事件
  getCity: function (e) {
    this.setData({
      cityText: e.detail.value,
      city: e.detail.code
    })
  },

  // 选择宿舍
  getDorm: function (e) {
    this.setData({
      index: e.detail.value
    })
  },

  // 选择班级
  getClass: function (e) {
    this.setData({
      count: e.detail.value
    })
  },

  // 提交
  submit: function (e) {
    // console.log(e.detail.value);
    // 表单数据对象
    var obj = e.detail.value;
    obj.id = wx.getStorageSync("uid");
    obj.cityText = obj.cityText.join(",");
    obj.city = this.data.city.join(",");
    obj.classId = this.data.classLs[obj.classId].id;
    obj.dormId = this.data.dormLs[obj.dormId].id || null;

    hs.post({
      url: api.applyStudy,
      data: obj
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})