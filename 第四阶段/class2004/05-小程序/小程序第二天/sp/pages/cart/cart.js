// pages/cart/cart.js

import util from "../../utils/util.js";
var carts = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 购物车列表
    carts: [
      { id: "1", title: '新鲜芹菜 半斤', image: '/assets/img/s5.png', num: 4, price: 0.01, selected: true },
      { id: "2", title: '素米 500g', image: '/assets/img/s6.png', num: 1, price: 0.03, selected: true }
    ],
    totalPrice: 0,           // 总价，初始为0
    selectAll: true    // 全选状态，默认全选
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.countPrice();
    carts = this.data.carts;
  },


  // 操作 使用委托 判断事件类型
  operate(e){
    var type = e.target.dataset.type;
    var i = e.currentTarget.dataset.index;
    
    if(!type) return;

    if(type === "minus"){
      if(carts[i].num == 1){
        util.toast("不能再减了");
      }else{
        carts[i].num--;
      }
    }else if(type === "add"){
      if (carts[i].num == 20) {
        util.toast("不能再加了,库存不够了");
      } else {
        carts[i].num++;
      }
    }else{
      carts.splice(i,1);
    }

    this.setData({
      carts: carts
    })

    this.countPrice();
  },
  
  // 选中和不选中
  changeCount(e){
    carts = carts.map(item => {
      if (!e.detail.value.includes(item.id)){
        item.selected = false;
      }else{
        item.selected = true;
      }
      return item;
    })
    this.setData({
      carts,
      selectAll: e.detail.value.length === carts.length
    })
    this.countPrice();
  },

  // 全选
  all(){
    var selectAll = !this.data.selectAll
    if(!selectAll){
      carts = carts.map(item => {
        item.selected = false;
        return item;
      })
    }else{
      carts = carts.map(item => {
        item.selected = true;
        return item;
      })
    }
    this.setData({
      carts,
      selectAll
    })
    this.countPrice();
  },

  // 计算总额
  countPrice(){
    var carts = this.data.carts;
    var totalPrice = 0;
    carts.forEach(item => {
      if (item.selected) {
        totalPrice += item.num * item.price;
      }
    })
    // 数值计算有浮点数,因为微信支付只能精确2位数,可使用膨胀还原法
    this.setData({
      totalPrice: Math.floor(totalPrice * 100) / 100
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})