// pages/my/my.js
import api from "../../utils/api.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse("button.open-type.getUserInfo"),
    ifShow: false,
    userInfo: {},
    status: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    // 查看本地是否有
    var userInfo = wx.getStorageSync("userInfo");

    if(userInfo){
      this.setData({
        userInfo: userInfo
      })
    }else{
      // 直接显示button获取
      this.setData({
        ifShow: true
      })
    }

  },

  // button获取用户信息
  getUserInfo(e){
    if(e.detail.errMsg === "getUserInfo:ok"){
      this.setData({
        userInfo: e.detail.userInfo,
        ifShow: false
      })

      wx.setStorageSync("userInfo", e.detail.userInfo);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    // 获取用户id 查看他的申请状态,显示相应的组件
    // var uid = wx.getStorageSync("uid");
    wx.getStorage({
      key: "uid",
      success(res){
        wx.request({
          url: api.status,
          data: {
            id: res.data
          },
          success(res) {
            // console.log(res);
            that.setData({
              status: res.data.data.status
            })
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})