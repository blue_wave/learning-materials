// pages/weather/weather.js

import hs from "../../utils/httpserve.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    city: ["河南省", "郑州市", "金水区"],
    weather: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getWeather();
  },


  // 切换城市
  getCity(e){
    this.setData({
      city: e.detail.value
    })
    this.getWeather(e.detail.value[1]);
  },

  // 获取城市相关信息
  getWeather(location){
    location = location || "郑州市";
    var that = this;
    hs.get({
      url: "https://free-api.heweather.net/s6/weather/now",
      data: {
        location: location,
        key: "d2f8dd9c09014e7fa9d73a3866f5fc91"
      }
    }).then(res => {
      // console.log(res);
      that.setData({
        weather: res.HeWeather6[0].now
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})