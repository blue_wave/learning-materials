
const designs = require("../../utils/data.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    maskTitle: "",
    ifh5: false,
    ifHidden: true,
    designArr: [],
    count: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(designs);
    // [0-21]  [[0-4],[5-9],[10-14],[15-19],[20-21]]
    // [[0,0.8],[1,1.8],[2,2.8],[3,3.8],[4,4.8]]
    var arr = [];
    var n = 0;
    arr[n] = [];
    for (var i = 0; i < designs.length; i++) {
      if (i < (n + 1) * 5) {
        arr[n].push(designs[i]);
      } else {
        arr[++n] = [];
        arr[n].push(designs[i]);
      }
    }
    // for(var i = 0; i < designs.length; i ++){
    //   var index = Math.floor(i / 5);
    //   if(i % 5 === 0){
    //     arr[index] = [];
    //   }
    //   arr[index].push(designs[i]);
    // }
    // console.log(arr);
    this.setData({
      designArr: arr
    })

  },

  // 显示遮罩
  showMask(e) {
    console.log(e)
    if (e.target.dataset.type) {
      this.setData({
        ifh5: true,
        maskTitle: "h5前端",
        ifHidden: false
      })
    } else {
      this.setData({
        maskTitle: "UI设计",
        ifHidden: false
      })
    }
  },

  // 关闭遮罩
  close() {
    this.setData({
      ifHidden: true
    })
  },
  stopTap() {
    return false;
  },

  // 改变swiper
  changeSwiper(e) {
    this.setData({
      count: e.target.dataset.index
    })
  },
  changeCount(e) {
    if (e.detail.source === "touch") {
      this.setData({
        count: e.detail.current
      })
    }
  },

  stopMove() {
    return;
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})