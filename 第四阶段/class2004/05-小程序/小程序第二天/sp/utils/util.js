const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

// 封装常用的 toast 方法
function toast(title,cb){
  wx.showToast({
    title: title,
    icon: cb ? "success" : "none",
    mask: true
  })
  if(cb){
    setTimeout(() => {
      cb();
    }, 1500)
  }
}

module.exports = {
  formatTime: formatTime,
  toast
}
