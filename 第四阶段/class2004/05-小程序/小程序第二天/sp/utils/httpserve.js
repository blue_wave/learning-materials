const utils = require("./util.js");
import qs from "./qs.js";
import api from "./api.js";

// 封装 get 和 post 请求
const hs = {
  /**
   * @params: { url, data }
   * @return then catch
   */
  get: function (params){
    if(!params.url){
      utils.toast("请输入请求地址");
      return;
    }
    return new Promise((response,reject) => {
      wx.request({
        url: params.url,
        data: Object.assign({}, params.data),
        success(res){
          response(res.data);
        },
        fail(err){
          reject(err);
        }
      })
    })
  },

  /**
   * @params: { url, data, cb}
   * @return: catch
   */
  post(params){
    if (!params.url) {
      utils.toast("请输入请求地址");
      return;
    }
    return new Promise((response,reject) => {
      wx.request({
        url: params.url,
        method: "POST",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: qs.stringify({ xyUserLite: params.data }, { allowDots: true }),
        success(res){
          if (res.data.code === "success"){
            utils.toast("请求成功", function (){
              // wx.switchTab({
              //   url: '/pages/my/my'
              // })
              wx.navigateBack({
                delta: 10
              })
            });
          }
        },
        fail(err){
          reject(err);
        }
      })
    })
  },

  // 获取学员信息
  usermsg(){
    // console.log(this);
    return this.get({
      url: api.detail,
      data: {
        id: wx.getStorageSync("uid")
      }
    })
  },

  // 获取宿舍列表
  getDorm(){
    return this.get({
      url: api.dormLs,
      data: { limit: 30 }
    })
  },

  // 获取班级列表
  getClass(){
    return this.get({
      url: api.classLs,
      data: { limit: 30 }
    })
  }
}

export default hs;

// axios.get({}).then(res);
