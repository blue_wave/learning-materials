# 微信小程序

## 购物车例子

## 生命周期

### 应用生命周期

```js
//app.js	小程序的生命周期函数在 app.js 中声明
App({
  onLaunch: function() { 
      //小程序初始化(全局只触发一次)
  },
  onShow: function() {
      //小程序显示
  },
  onHide: function() {
      //小程序隐藏
  },
  onError: function(msg) {
      //小程序错误
  },
})
```

+ 用户首次打开小程序，触发  `onLaunch`（全局只触发一次）。

+ 小程序初始化完成后，触发 `onShow` 方法，监听小程序显示。

+ 小程序从前台进入后台，触发 `onHide` 方法。

+ 小程序从后台进入前台显示，触发 `onShow` 方法。

+ 小程序后台运行一定时间，会被销毁。

+ 小程序出错，触发`onError`

![image.png](https://upload-images.jianshu.io/upload_images/6784887-5c86b6a1b4a827af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> 前台、后台定义： 当用户点击右上角关闭，或者按了设备返回键离开微信小程序，小程序并没有直接销毁，而是进入了后台；当再次进入微信或再次打开小程序，又会从后台进入前台

### 页面的生命周期

- 页面未在页面栈中，初次加载页面，触发`onLoad`方法。
- 页面载入后触发`onShow`方法，显示页面。
- 页面未在页面栈中，初次加载页面，会触发`onReady`方法，渲染页面元素和样式
- 离开本页面时，触发`onHide`方法。
- 当页面从页面栈中被销毁时，触发`onUnload`方法

![image.png](https://upload-images.jianshu.io/upload_images/6784887-e2500e9e2413908a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 页面路由

用于页面间路径跳转

页面栈以栈（先进后出）的形式维护页面与页面之间的关系，路由更改了页面栈的层数，小程序页面栈最多10层

小程序提供了`getCurrentPages()`函数获取页面栈，第一个元素为首页，最后一个元素为当前页面

### API进行页面间跳转

#### `wx.switchTab` 

跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面，页面栈内仅剩当前页。

```js
wx.switchTab({
  url: '/pages/index/index'
})
```



#### `wx.reLaunch`

关闭所有页面，打开到应用内的某个页面，页面栈内仅剩当前页面。

```js
// index Tab页面 --> A页面 --> B页面 --> C页面 当前 C 页面

// 显示 A 页面
wx.reLaunch({
  url: '../A/A'
})
// 显示index Tab页面
wx.reLauch({
    url: "/pages/index/index"
})
```



#### `wx.redirecTo`

关闭当前页面，跳转到应用内的某个页面，页面栈数量不增不减。但是不允许跳转到 tabbar 页面。

```js
// index Tab页面 --> A页面 --> B页面 --> C页面 当前 C 页面

// 重定向到 A页面,此时页面栈  [index页面,A页面,B页面,A页面]
wx.redirecTo({
  url: '../A/A'
})
```



#### `wx.navigateTo`

保留当前页面，跳转到应用内的某个页面，页面栈数量 + 1。但是不能跳到 tabbar 页面。

```js
// 当前 A页面, 跳转到 B页面
wx.navigateTo({
  url: '../B/B'
})
```



#### `wx.navigateBack`

关闭当前页面，返回上一页面或多级页面 页面栈数量 - n。可通过 [getCurrentPages](https://developers.weixin.qq.com/miniprogram/dev/reference/api/getCurrentPages.html) 获取当前的页面栈，决定需要返回几层。

```js
// A页面 --> B页面 --> C页面 当前 C 页面

wx.navigateBack(); // 返回 B 页面

// 返回A页面
wx.navigateBack({
  delta: 2	// 返回的页面数,如果大于现有页面数,则返回页面栈第一个页面
})
```

### 组件方式进行页面间跳转

```vue
<!-- 
  url 要跳转的页面路径
  open-type 跳转方式 对应上面的跳转方法
	navigate、redirect、switchTab、reLaunch、navigateBack
  delta 当 open-type 为 'navigateBack' 时有效，表示回退的层数,默认为 1
-->
<navigator url="pages/logs/index" open-type="navigate">跳转</navigator>
```

## 网络请求

使用 `wx.request` 方法，[详情参见官方API](https://developers.weixin.qq.com/miniprogram/dev/api/network/request/wx.request.html)

](https://developers.weixin.qq.com/miniprogram/dev/api/network/request/wx.request.html)

```js
// 示例: 请求豆瓣最新上映电影列表
wx.request({
  url: 'http://www.bufantec.com/api/douban/movie/in_theaters',
  data: {
    start: "1",
    limit: "20"
  },
  header: {
    'content-type': 'application/json' // 默认值
  },
  success(res) {
    console.log(res.data)
  }
})
```

获取豆瓣电影及电影详情

**`wx.request` 返回数据说明: 第一个data 是微信返回的信息,data 里面的 data 才是后台返回给我们的数据**

## 请求封装

```js
import url1 from "./baseurl"

import qs from "./qs"

var obj = {

 get(path,params){

  return new Promise((resolve,rejected)=>{

   wx.request({

    url:url1 + path,

    data:params,

    success(res){

     resolve(res)

    },

    fail(val){

     rejected(val)

    }

   })

  })

 },

 post(path,params){

  return new Promise((resolve,rejected)=>{

   wx.request({

    url: url1 + path,

    data:qs.stringify(params,{ allowDots: true}),

    method:"POST",

    header:{

     "content-type":"application/x-www-form-urlencoded"

    },

    success(val){

     resolve(val)

    },

    fail(val){

     rejected(val)

    }

   })

  })

 }

}



export default obj
```

baseUrl.js

```
var url = "http://bufantec.com"

export default url
```

## 页面通讯

进行小程序页面间的通信

#### 全局变量

`app.globalData`

```
// 在app.js中设置对象 globalData{}
// 在要传值的页面引入 var app = getApp();
app.globalData.name = "王安石";
// 在要接受值的页面引入  var app = getApp();
console.log(app.globalData.name);
```

#### 本地缓存

**注意**:不同与浏览器本地缓存storage ，小程序本地缓存分为同步和异步操作

```
// 在要传值的页面
wx.setStorageSync("name","陆游");
// 在要接受值的页面
wx.getStorageSync("name");
```

#### 使用路由

```
// 在要跳转的路由后面加上 ?key=value&key1=value1 的形式;注意: wx.switchTab 中的 url 不能传参数。
// 要跳转的页面可以通过onload函数的形参options接受上一个页面传递的值

wx.navigateTo({
   url:'../pageD/pageD?name="李白"'
})

// B页面-接收数据 通过onLoad的option...
Page({
  onLoad: function(option){
 	console.log(option.name)
  }
})
```

