# 微信小程序

## 第一章 小程序介绍

微信小程序「weixinxiaochengxu」，简称小程序，英文名MINI PROGRAM ，小程序是一种**运行在微信**上的**无需下载安装即可使用**的应用，如今几乎每台智能手机都会安装微信程序，微信的用户也是遍布全球，微信拥有海量的用户，可想而知微信小程序拥有很大的潜力。

2016年9月21日，微信正式开始内测。在微信生态下，这个不需要下载安装、用完即走的小程序引起了大家广泛的关注。腾讯云也正式上线了小程序的解决方案，提供了小程序在云端的技术方案。

2016年1月11日，微信之父张小龙公开亮相，解读了微信四大价值观。张小龙指出，越来越多的产品注册公众号，因为这里开发、推广传播成本比较低。但拆分出来的服务号功能做的却并不理想，所有微信内部正在研究一个新的形态，叫《微信小程序》。

2017年1月9日0点，微信小程序正式低调上线，为开发提供了全新的组件、API技术，为用户开启不一样的体验模式。

张小龙曾说：“让创造发挥价值，好产品应该是用完即走，微信应该是给用户提供便利，而非浪费时间。希望用户在微信里看到的都是自己愿意看到的东西，也希望用户能够留出来更多的时间去做其他的事情”。所以能看出微信的目的就是想把用户实用微信的动作都集中在微信上，而微信小程序也就将是另一个App Store。

## 第二章 如何创建一个小程序

学习任何语言都是从最基本的「Hello World」开始，所以学习小程序开发之前，我们先来创建一个自己的小程序，同时微信公众平台为我们提供了小程序接入指南

https://mp.weixin.qq.com/debug/wxadoc/introduction/index.html?t=2017117

### 2.1 成为小程序开发者

我们可以通过微信公众平台里的小程序入口 https://mp.weixin.qq.com/cgi-bin/wx 来注册成为小程序开发者，注册页面如图1-1所示。

![img](https://weapp.520wcf.com/assets/%E5%9B%BE1-1.png) 图2-1 小程序注册页面

填写完邮箱、密码等信息，点击注册按钮，激活邮箱如图1-2所示。

![img](https://weapp.520wcf.com/assets/%E5%9B%BE1-2.png) 图2-2 激活邮箱页面

点击激活链接后，继续下一步的注册流程。请选择主体类型选择，完善主体信息和管理员信息。

![img](https://res.wx.qq.com/wxdoc/dist/assets/img/03271.7befb864.png) 图3-3 登记用户信息页面

主体类型说明如下：

|    **帐号主体**    |                           **范围**                           |
| :----------------: | :----------------------------------------------------------: |
|        个人        |             18岁以上有国内身份信息的微信实名用户             |
|        企业        |                企业、分支机构、企业相关品牌。                |
| 企业（个体工商户） |                         个体工商户。                         |
|        政府        | 国内、各级、各类政府机构、事业单位、具有行政职能的社会组织等。目前主要覆盖公安机构、党团机构、司法机构、交通机构、旅游机构、工商税务机构、市政机构等。 |
|        媒体        |           报纸、杂志、电视、电台、通讯社、其他等。           |
|      其他组织      |             不属于政府、媒体、企业或个人的类型。             |

参考 [https://developers.weixin.qq.com/miniprogram/introduction/#%E5%A1%AB%E5%86%99%E4%B8%BB%E4%BD%93%E4%BF%A1%E6%81%AF](https://developers.weixin.qq.com/miniprogram/introduction/#填写主体信息)

完成注册后，可以登录公众平台 [https://mp.weixin.qq.com](https://mp.weixin.qq.com/) 完善小程序信息

### 2.2 安装开发者编辑器

开发小程序我们需要用到「微信WEB开发者工具」，可以通过如下地址直接下载编辑器 https://mp.weixin.qq.com/debug/wxadoc/dev/devtools/download.html ，官方为大家提供了windows 64 、 windows 32 、 MAC三个版本，根据自己的电脑选择适合的开发工具下载。

安装完成后之后运行即可，启动后需要开发者使用微信扫描二维码验证登录，如图1-5。

![img](https://weapp.520wcf.com/assets/%E5%9B%BE1-5.png) 图2-2 开发者工具登录页面

### 2.3 创建一个小程序



![image-20200720151156229](./imgs/创建.png)

### 2.4 微信小程序appid

appid相当于你的小程序在微信中的 ‘身份证’ ，有了它，微信客户端才能确定你的 ‘小程序’ 的身份，并使用微信提供的高级接口。

## 第三章 体验小程序

### 3.1 小程序代码结构和基本配置介绍

#### 3.1.1 pages

微信小程序每一个页面由四个文件组成，分别是js 页面逻辑 、 json 单个页面的配置 、wxml 页面内容 、wxss  样式

`pages`文件夹里是小程序的各个页面，每个界面一般都由`.wxml`、`.wxss`、`.js`、`.json`四个文件组成，四个文件必须是相同的名字和路径

+ `.js` 是页面的脚本代码，通过`Page()`函数注册页面。可以指定页面的初始数据、生命周期、事件处理等

+ `.wxml` 是页面的布局文件，只能使用微信定义的组件

+ `.wxss` 是样式表

+ `.json` 页面配置文件，设置与页面顶部显示内容的相关信息

  

#### 3.1.2 utils

存放项目当中js模块的区域

  js 模块通过`export default` 对外暴露接口，其它使用的地方通过 `import`  进行引用

#### 3.1.3 app.js   

监听并处理小程序的生命周期、声明全局变量

  页面的`.js`文 件可以通过`var app = getApp()` 获取其实例，调用其中定义的方法和变量，但不可以调用生命周期的方法

#### 3.1.4 app.json

小程序全局配置

  ```json
  // app.json 配置介绍
  "pages": [ // 配置小程序的组成页面，第一个代表小程序的初始页面
    "pages/index/index", //不需要写index.wxml,index.js,index,wxss,框架会自动寻找并整合
    "pages/logs/logs"
  ],
  "window": { // 设置小程序的状态栏、标题栏、导航条、窗口背景颜色
    "navigationBarBackgroundColor": "#ffffff", //顶部导航栏背景色
    "navigationBarTextStyle": "black", //顶部导航文字的颜色 black/white
    "navigationBarTitleText": "微信接口功能演示", //顶部导航的显示文字
    "backgroundColor": "#eeeeee", //窗口的背景色
    "backgroundTextStyle": "light", //下拉背景字体、loading 图的样式，仅支持 dark/light
    "enablePullDownRefresh": "false"， //是否支持下拉刷新 ，不支持的话就直接不写！
    "disableScroll": true, //  设置true不能上下滚动，true/false，注意！只能在 page.json 中有效，无法在 app.json 中设置该项。
  },
  "tabBar": { //底部tab或者顶部tab的表现，是个数组，最少配置2个，最多5个
      //只有定义为 tabBar 的页面，底部才有 tabBar
    "list": [{ //设置tab的属性，最少2个，最多5个
      "pagePath": "pages/index/index", //点击底部 tab 跳转的路径
      "text": "首页", //tab 按钮上的文字
      "iconPath": "../img/a.png", //tab图片的路径，本地路径
      "selectedIconPath": "../img/a.png" //tab 在当前页，也就是选中状态的路径
    }, {
      "pagePath": "pages/logs/logs",
      "text": "日志"
    }],
    "color": "red", //tab 的字体颜色
    "selectedColor": "#673ab7", //当前页 tab 的颜色，也就是选中页的
    "backgroundColor": "#2196f3", //tab 的背景色
    "borderStyle": "white", //边框的颜色 black/white
    "position": "bottom" //tab处于窗口的位置 top/bottom
    },
  "networkTimeout": { //默认都是 60000 秒一分钟
      "request": 10000, //请求网络超时时间 10000 秒
      "downloadFile": 10000， //链接服务器超时时间 10000 秒
      "uploadFile": "10000", //上传图片 10000 秒
      "downloadFile": "10000" //下载图片超时时间 10000 秒
    },
  "debug": true //项目上线后，建议关闭此项，或者不写此项
  ```

  

#### 3.1.5 app.wxss	

小程序全局样式(引入外部样式的时候只在这里面引入即可)

#### 3.1.6 project.config.json

微信开发者工具的配置信息 不需要自己配置 自动生成的文件

#### 3.1.7 siteMapLocation 

[详情参见此处](https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/sitemap.html) 一般不用更改,在此不做说明

**注意:** 定义在`app.wxss`中的全局样式，作用于每个页面。定义在`page`的`.wxss`文件只作用于对应的页面，会覆盖`app.wxss`中相同的选择器



## 第四章 小程序内容详解

### 4.1 wxml

#### 4.1.1 数据绑定

> 传统的视图和数据绑定

![image.png](https://upload-images.jianshu.io/upload_images/6784887-0e05aabacd9f1974.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/940/format/webp)

> 小程序中视图和数据绑定 对象数据变化直接影响视图，视图变化必须同过事件驱动才能改变对象数据

![image.png](https://upload-images.jianshu.io/upload_images/6784887-9f93715f3657e148.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/940)

> WXML中的动态数据均来自对应的 `Page` 的 `data`。数据绑定使用 Mustache 语法（双大括号将变量包起来）。

+ 基本使用

  ```vue
  <view> {{ message }} </view>
  ```

  ```js
  Page({
    data: {
      message: 'Hello MINA!',
      name: "李白",
      status: 3
    }
  })
  ```

+ 绑定属性 

  ```vue
  <view data-name="{{name}}"></view>
  ```

+ 支持表达式

  ```vue
  <view hidden="{{status > 5}}">是否显示</view>
  <view class="{{status === 3 ? 'red' : 'blue'}}"></view>
  <view> {{'hello' + name}}</view> 
  ```

+ 更多使用方式  [查看](https://developers.weixin.qq.com/miniprogram/dev/reference/wxml/data.html)

#### 4.1.2 列表渲染

```vue
<view wx:for="{{array}}">
  {{index}}: {{item.message}}
</view>
```

```js
Page({
  data: {
    array: [
    {
      message: 'foo',
    },
    {
      message: 'bar'
    }]
  }
})
```

> 使用 `wx:for-item` 可以指定数组当前元素的变量名，使用 `wx:for-index` 可以指定数组当前下标的变量名：

```vue
<view wx:for="{{array}}" wx:key="{{idx}}" wx:for-index="idx" wx:for-item="itemName">
  {{idx}}: {{itemName.message}}
</view>
```

#### 4.1.3 条件渲染

```vue
<view wx:if="{{condition}}"> True </view>
```

> 也可以用 `wx:elif` 和 `wx:else` 来添加一个 else 块：

```vue
<view wx:if="{{isLogin}}">已登录</view>
<view wx:else>请登录</view>

<view wx:if="{{module == 'A'}}"> A </view>
<view wx:elif="{{module == 'B'}}"> B </view>
<view wx:else> C </view>
```

#### 4.1.4 `block`的使用

> `<block/>` 并不是一个组件，它仅仅是一个包装元素，不会在页面中做任何渲染，只接受控制属性。

```vue
<block wx:for="{{array}}" wx:key="{{index}}">
	<text>{{item}}</text>
</block>
<block wx:if="{{flag}}">
	<text>看flag是否为true</text>
</block>
```

#### 4.1.5 组件属性

+ `ID/class`  ID名/类名

+ `style`  行内样式

+ `hidden`   组件是否显示

+ `data-*` 自定义属性

  可以通过事件的`e.target.dataset.*`来获取设置的自定义属性

  ```vue
  <!-- wxml -->
  <view bindtap="clickMe" data-testId="{{testId}}"></view>
  
  // js
  Page({
      clickMe: function(event) {
          var testId = event.target.dataset.testid;
      }
  })
  ```

  

+ **注意：不要直接写 hidden="false" ， 其计算结果是一个字符串，转成 boolean 类型后代表真值。现在可以不带花括号，建议加上**

+ **`wx:if`和`hidden`都可以实现组件的显示和隐藏效果；区分场景：如果需要频繁切换，用 `hidden` 更好；如果执行条件不大可能改变则使用 `wx:if` 较好**

#### 4.1.6 组件类型 

[参见官方API](https://developers.weixin.qq.com/miniprogram/dev/component/)

### 4.2 wxss

#### 4.2.1 尺寸单位 `rpx` 

可以根据屏幕宽度进行自适应。小程序规定屏幕宽为750rpx。如在 iPhone6 上，屏幕宽度为375px，共有750个物理像素，则750rpx = 375px = 750物理像素，1rpx = 0.5px = 1物理像素。

![image.png](https://upload-images.jianshu.io/upload_images/6784887-cc603a9c54cc1dbb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

#### 4.2.2 样式导入

使用`@import`语句可以导入外联样式表，`@import`后跟需要导入的外联样式表的相对路径，用`;`表示语句结束。

```scss
/** common.wxss **/
.small-p {
  padding:5px;
}
```

```scss
/** app.wxss **/
@import "common.wxss";
.middle-p {
  padding:15px;
}
```

#### 4.2.3 选择器

wxss 选择器的类型和优先级与 css 中的相同，但是伪类选择器不会在调试器的 wxml 中显示

**注意：本地资源图片无法通过 WXSS 获取，可以使用网络图片，或者 base64，或者使用 image 标签**



## 第五章 小程序的事件

> 事件是视图层到逻辑层的通讯方式，可以将用户的行为反馈到逻辑层进行处理。事件可以绑定在组件上，触发事件后，就会执行逻辑层中对应的事件处理函数。小程序的事件对象可以查看自定义属性的值

### bind和catch

小程序通过 `bind*/catch*` (*代表事件类型) 给组件添加事件；`bind`方式绑定的事件会向上冒泡，`catch`方式绑定的事件不会向上冒泡

```vue
<!-- 点击 inner view 会先后调用handleTap3和handleTap2(因为tap事件会冒泡到 middle view，而 middle view 阻止了 tap 事件冒泡，不再向父节点传递)，点击 middle view 会触发handleTap2，点击 outer view 会触发handleTap1 -->

<view id="outer" bindtap="handleTap1">
  outer view
  <view id="middle" catchtap="handleTap2">
    middle view
    <view id="inner" bindtap="handleTap3">
      inner view
    </view>
  </view>
</view>
```



常见事件类型：`toushstart touchmove touchcancel touchend tap longpress longtap transitionend animationstart animationiteration animationend touchforcechange` 

小程序表单、swiper等组件有自己的事件类型，详情参见官方API各组件详细内容事件介绍

通过事件改变页面 `data` 对象内容的方式

```vue
<!-- index.wxml -->
<button bindtap="changeCount">{{count}}</button>

<!--  index.js -->
page({
  data{
    count: 0
  },
  changeCount: function (){
	this.setData({
	  count: this.data.count ++
	})
  }
})
```



### 事件的捕获阶段

需要在捕获阶段监听事件时，可以采用`capture-bind`、`capture-catch`关键字，后者将中断捕获阶段和取消冒泡阶段

在下面的代码中，点击 inner view 会先后调用`handleTap2`、`handleTap4`、`handleTap3`、`handleTap1`

```vue
<view id="outer" bind:touchstart="handleTap1" capture-bind:touchstart="handleTap2">
  outer view
  <view id="inner" bind:touchstart="handleTap3" capture-bind:touchstart="handleTap4">
    inner view
  </view>
</view>
```

如果将上面代码中的第一个`capture-bind`改为`capture-catch`，将只触发`handleTap2`

```vue
<view id="outer" bind:touchstart="handleTap1" capture-catch:touchstart="handleTap2">
  outer view
  <view id="inner" bind:touchstart="handleTap3" capture-bind:touchstart="handleTap4">
    inner view
  </view>
</view>
```

## 其他的小知识

### 小程序自定义轮播小点的样式

```scss
// 页面  <swiper class="square-dot" indicator-dots indicator-active-color="#fff">...</swiper>

swiper.square-dot .wx-swiper-dot {
	background-color: #000;
	opacity: 0.4;
	width: 10rpx;
	height: 10rpx;
	border-radius: 20rpx;
	margin: 0 8rpx !important;
}

swiper.square-dot .wx-swiper-dot.wx-swiper-dot-active{
	opacity: 1;
	width: 30rpx;
}
```



### 小程序获取当前页面并设置上一个页面内data的某些值

```js
// 获取页面栈,数组第一个是首页,最后一个是当前页
var pages = getCurrentPages();
// 当前页面
var currPage = pages[pages.length - 1];
// 当前页面设置值
currPage.setData({
    wait: true
})
// 前一个页面
var prevPage = pages[pages.length -2];
// 前一个页面设置值
prevPage.setData({
    wait: true
})
```

### 扩展的样式

```css
/* 组件添加向右的箭头的样式 */
.arrow {
  position: relative;
}

.arrow:after {
  position: absolute;
  top: 50%;
  right: 28rpx;
  margin-top: -8rpx;
  display: block;
  content: " ";
  height: 18rpx;
  width: 18rpx;
  border-width: 3rpx 3rpx 0 0;
  border-color: #888888;
  border-style: solid;
  transform: matrix(0.71, 0.71, -0.71, 0.71, 0, 0);
}

/* 扩散元素自身的颜色的阴影写法 */
.shadow-blur {
	position: relative;
}

.shadow-blur::before {
	content: "";
	display: block;
	background: inherit;
	filter: blur(10rpx);
	position: absolute;
	width: 100%;
	height: 100%;
	top: 10rpx;
	left: 10rpx;
	z-index: -1;
	opacity: 0.4;
	transform-origin: 0 0;
	border-radius: inherit;
	transform: scale(1, 1);
}

/* 两端翘起的阴影效果 */
.shadow-warp {
	position: relative;
    background-color: #fff;
	box-shadow: 0 0 10rpx rgba(0, 0, 0, 0.1);
}

.shadow-warp:before,
.shadow-warp:after {
	position: absolute;
	content: "";
	top: 20rpx;
	bottom: 30rpx;
	left: 20rpx;
	width: 50%;
	box-shadow: 0 30rpx 20rpx rgba(0, 0, 0, 0.2);
	transform: rotate(-3deg);
	z-index: -1;
}

.shadow-warp:after {
	right: 20rpx;
	left: auto;
	transform: rotate(3deg);
}
```

### 去除button的默认边框

```css
/* button的边框样式是通过::after方式实现的，如果在button上定义边框就会出现两条边框线，所以我们可以使用::after的方式去覆盖默认值 */

button::after {
  border: none;
}
```



-------------

 

## 须知

+ 页面需要在`app.json`中进行注册，否则不能访问
+ `app.json`中`pages`数组的第一项代表小程序的初始页面，小程序中新增/减少页面，都需要对 `pages` 数组进行修改
+ 直接修改 `this.data`无法改变页面的状态，还会造成数据不一致
+ 不要尝试修改页面栈，会导致路由以及页面状态错误
+ 小程序页面最多同时打开 10 个，如果交互流程较长难以支持
+ `tabBar` 上面的按钮 `iconPath` 不支持网络路径，`icon`大小限制为`40kb`，官方推荐尺寸是 81* 81
+ `setStorage` 本地缓存最大为10MB
+ 编译后的代码包大小需小于 `2MB`，否则代码包将上传失败
+ 为什么脚本内不能使用window等对象：因为页面的脚本逻辑是在`JsCore`中运行，`JsCore` 是一个没有窗口对象的环境，所以不能在脚本中使用`window`，也无法在脚本中操作组件





豆瓣电影图片无法显示：

1、首先去掉获取的图片地址的 `https://`

2、在图片地址前面拼接上：`https://images.weserv.nl/?url=`

```js
//  原来
"https://img3.doubanio.com/view/subject/l/public/s2955123.jpg"
//  修改后
"https://.weserv.nl/?url=img3.doubanio.com/view/subject/l/public/s2955123.jpg"
```



