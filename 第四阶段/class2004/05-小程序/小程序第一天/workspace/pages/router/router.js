// pages/router/router.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  goHome(){
    // wx.reLaunch  跳转到tabbar页面 switchTab
    wx.reLaunch({
      url:"/pages/home/home"
    })
  },
  goInfo(){
    wx.reLaunch({
      url:"/pages/info/info"
    })
  },
  goHome1(){
    // wx.redirectTo  不能跳转到tabbar页面 switchTab
    wx.redirectTo({
      url:"/pages/home/home"
    })
  },
  goInfo1(){
    wx.redirectTo({
      url:"/pages/info/info"
    })
  },
  goHome2(){
    // wx.redirectTo  不能跳转到tabbar页面 switchTab
    wx.navigateTo({
      url:"/pages/home/home"
    })
  },
  goInfo2(){
    wx.navigateTo({
      url:"/pages/info/info"
    })
  },
  goHome3(){
    // wx.redirectTo  不能跳转到tabbar页面 switchTab
    wx.switchTab({
      url:"/pages/home/home"
    })
  },
  goInfo3(){
    wx.switchTab({
      url:"/pages/info/info"
    })
  },
  back(){
    wx.navigateBack({
      delta:1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})