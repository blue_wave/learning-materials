//index.js
//获取应用实例


Page(
  {
    data:{
      num:2,
      isClassOn:false,
      isItalic:"normal",
      indicatorDots: true,
      background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
      vertical: false,
      autoplay: false,
      interval: 2000,
      duration: 500,
      isMaskShow:false,
      list:[
        {
          name:"苹果",
          price:30,
          id:1,
          childList:[
            {
              name:"苹果1",
              id:6
            }
          ]
        },
        {
          name:"香蕉",
          price:40,
          id:2,
          childList:[
            {
              name:"香蕉1",
              id:7
            }
          ]
        },
        {
          name:"橘子",
          price:50,
          id:3,
          childList:[
            {
              name:"橘子1",
              id:8
            }
          ]
        },
      ]
    },
    inc(val){
      // 小程序的数据时单向绑定
     // this.num++
     console.log(this.data.num)
      this.setData({
        num:this.data.num+val.target.dataset.id
      })
    },
    changeColor(){
      this.setData({
        isClassOn:!this.data.isClassOn
      })
    },
    events(e){
      console.log(e.target.dataset.index)
    },
    toggleMask(){
      this.setData({
        isMaskShow:!this.data.isMaskShow
      })
    },
    boxEvents(){

    }
  }
)
