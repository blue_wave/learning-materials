// pages/car/car.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isChecked:false,
    isChecked1:false,
    isChecked2:false,
    isAllChecked:false,
    totals:0,
    list:[
      {
        name:"苹果",
        price:2,
        num:3,
        id:1,
        isChecked:false
      },
      {
        name:"香蕉",
        price:3,
        num:5,
        id:2,
        isChecked:true
      },
    ]
  },
  aa(ee){
    console.log(ee);
    // eee.detail.value 保存的时选中之后的checkbox
  },
  selectedList(e){
    // 先判断列表项 是否全部选中 
    console.log(e.detail.value);
    this.data.list.forEach(ele =>{
      if(e.detail.value.includes(JSON.stringify(ele.id))){
        ele.isChecked = true
      }else{
        ele.isChecked = false
      }
    })
    if(e.detail.value.length == this.data.list.length){

      this.setData({
        isAllChecked:true
      })
    }else{
      this.setData({
        isAllChecked:false
      })
    }
    this.totalPrice()
  },
  totalPrice(){
    var totals = this.data.list.reduce((total,current)=>{
      if(current.isChecked){
        return total+ current.price*current.num
      }else{
        return total
      }
    },0)
    this.setData({
      totals:totals
    })
  },
  selectedAll(){
    var bol = this.data.list.every(ele =>ele.isChecked);
    this.data.list.forEach(ele =>{
      ele.isChecked = !bol
    })
    this.setData({
      list: this.data.list
    })
    this.totalPrice()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.totalPrice()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  dec(e){
    console.log(e.target.dataset);
    var item = this.data.list.find(ele =>ele.id == e.target.dataset.id);
    if(item.num == 0){
      return 
    }
    item.num -- ;
    this.setData({
      list:this.data.list
    })
    this.totalPrice()
  },
  inc(e){
    var item = this.data.list.find(ele =>ele.id == e.target.dataset.id);
   
    item.num ++ ;
    this.setData({
      list:this.data.list
    })
    this.totalPrice()
  },
  del(e){
   var index =  this.data.list.findIndex(ele =>ele.id == e.target.dataset.id)
    this.data.list.splice(index,1);
    this.setData({
      list:this.data.list
    })
    this.totalPrice()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})