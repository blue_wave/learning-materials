// pages/home/home.js
import http from "../../utils/http.js"
//  http.get("/a",{id:1})
//  .then(res =>{

//  })
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   * 类似与vue的created 
   */
  info(){
    var mid = 1292052;
    http.get("/api/douban/movie/subject",{
      mId:mid
    })
    .then(res =>{
      console.log(res)
    })
    // wx.request({
    //   url: 'http://bufantec.com/api/douban/movie/subject', //仅为示例，并非真实的接口地址
    //   data:{
    //     mId:mid
    //   },
    //   success (res) {
    //     console.log(res.data)
    //     _this.setData({
    //       list:res.data.data.list
    //     })
    //   }
    // })
  },
  login(){
    http.post("/api/test/user/doLogin",{
      username:"admin",
      password:"123"
    })
    .then(res =>{
      console.log(res)
    })
    // wx.request({
    //   url: 'http://bufantec.com/api/test/user/doLogin', //仅为示例，并非真实的接口地址
    //   data:{
    //     username:"admin",
    //     password:"123"
    //   },
    //   method:"POST",
    //   header:{
    //     "content-type":"application/x-www-form-urlencoded"
    //   },
    //   success (res) {
    //     console.log(res.data)
    //     // _this.setData({
    //     //   list:res.data.data.list
    //     // })
    //   }
    // })
  },
  goInfo(e){
    console.log(e)
    // 小程序当中 没有动态路由这个概念
    wx.navigateTo({
      url:`/pages/info/info?id=${e.currentTarget.dataset.mid}&name="小明"`
    })
  },
  onLoad: function (options) {
    // options  用于页面间传值的获取 a=>b?id=1 this.$route.query.id
    console.log("onLoad");
    // wx.request  默认时get请求
    var _this = this;
    wx.request({
      url: 'http://bufantec.com/api/douban/movie/top250', //仅为示例，并非真实的接口地址
      success (res) {
        console.log(res.data)
        _this.setData({
          list:res.data.data.list
        })
        
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   * 类似于vue的mounted
   */
  onReady: function () {
    console.log("onReady");
  },

  /**
   * 生命周期函数--监听页面显示
   * 
   */
  onShow: function () {
    console.log("onShow");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log("onHide");
  },

  /**
   * 生命周期函数--监听页面卸载
   * 类似于vue 的destoryed
   */
  onUnload: function () {
    console.log("onUnload");
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})