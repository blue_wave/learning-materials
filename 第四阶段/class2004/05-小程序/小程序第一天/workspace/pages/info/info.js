// pages/info/info.js
import http from "../../utils/http.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.init(options.id)
  },
  init(id){
    http.get("/api/douban/movie/subject",{
      mId:id
    })
    .then(res =>{
      console.log("详情数据获取成功",res)
    })
  },
  goHome(){
    // 需要跳转到tab对应的页面
    // wx.navigateTo({
    //   url:"/pages/home/home"
    // })
    wx.switchTab({
      url:"/pages/home/home"
    })
  },
  back(){
    wx.navigateBack({
      delta:1
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})