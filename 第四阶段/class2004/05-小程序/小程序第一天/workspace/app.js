//app.js
// App  初始化 小程序
App({
  // 只要首次进入到小程序的时候才会执行的钩子函数 只执行一次
  // onLaunch 用于判断用户是否登录
  // onLaunch: function () {
  //   console.log("onLaunch执行了")
  // },
  // // onShow  小程序显示的时候执行的钩子函数
  // // onShow onHide 在应用以及页面都会执行的钩子函数
  // onShow(){
  //   console.log("onShow执行了")
  // },
  // // onHide  小程序隐藏的时候执行的钩子函数
  // onHide(){
  //   console.log("onHide执行了")
  // },
  // onError(){
  //   console.log("onError执行了")
  // },
  globalData: {
    userInfo: null
  }
})