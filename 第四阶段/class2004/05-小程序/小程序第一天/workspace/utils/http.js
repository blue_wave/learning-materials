import baseUrl from "./url.js"

var obj = {
  get(url,params){
    return  new Promise((resolve,rejected)=>{
      wx.request({
        url: baseUrl + url,
        data:params,
        success(res){
          resolve(res)
        }
      })
    })
  },
  post(url,params){
    return new Promise(function(resolve,rejected){
      wx.request({
        url: baseUrl+url,
        method:"POST",
        data:params,
        header:{
          "content-type":"application/x-www-form-urlencoded"
        },
        success(res){
          resolve(res)
        }
      })
    })
  }
}

export default obj