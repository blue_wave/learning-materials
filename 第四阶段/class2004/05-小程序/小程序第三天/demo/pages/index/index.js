//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isLogin:false,
    imgPath:"",
    num:2,
    isShow:true
  },
  bindGetUserInfo(e){
    console.log(e)
  },
  parentEvents(val){
    console.log(val);
    this.setData({
      num:val.detail.num
    })
  },
  getId(){
    console.log(app.globalData.id)
  },
  scanCode(){
    wx.scanCode({
      onlyFromCamera:true,
      success (res) {
        console.log(res.result)
      }
    })
  },
  selectedImg(){
    var _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        console.log(tempFilePaths[0]);
        wx.uploadFile({
          url: 'http://bufantec.com/api/test/user/upload', //仅为示例，非真实的接口地址
          filePath: tempFilePaths[0],
          name: 'file',
          timeout:5000,
          success (res){
            const data = res.data;
            var obj = JSON.parse(res.data).data;
            console.log(res.data)
            _this.setData({
              imgPath:"http://bufantec.com/"+obj
            })
            //do something
          }
        })
        // wx.request({
        //   url: 'http://bufantec.com/api/test/user/upload',
        //   header:{
        //     'content-type':"multipart/form-data"
        //   },
        //   method:"POST",
        //   data:{
        //     file:tempFilePaths[0]
        //   },
        //   success(res){
        //     console.log(res)
        //   }
        // })
      }
    })
  },
  onShow(){
    console.log(1111);
    var key = wx.getStorageSync('key')
    // wx.getStorage({
    //   key: 'key',
    //   success (res) {
    //     console.log(res.data)
    //   }
    // })
    if(key){
        this.setData({
          isLogin:true
        })
    }else{
      this.setData({
        isLogin:false
      })
    }
    // if(app.globalData.id){
    //   this.setData({
    //     isLogin:true
    //   })
    // }else{
    //   this.setData({
    //     isLogin:false
    //   })
    // }
  },
  removeKey(){
    // wx.removeStorageSync('key')
    wx.clearStorageSync()
  },
  //事件处理函数
  con(){
    // 使用这个接口的时候 一定要注意失败的回调
    wx.getUserInfo({
      success: function(res) {
       
        var userInfo = res.userInfo
        var nickName = userInfo.nickName
        var avatarUrl = userInfo.avatarUrl
        var gender = userInfo.gender //性别 0：未知、1：男、2：女
        var province = userInfo.province
        var city = userInfo.city
        var country = userInfo.country
        console.log("获取用户信息成功",userInfo)
      },
      fail(){
        wx.showToast({
          title: '获取用户信息失败',
          icon:"none",
          duration: 2000
        });
        console.log("获取用户信息失败")
      }
    })
  }
})
