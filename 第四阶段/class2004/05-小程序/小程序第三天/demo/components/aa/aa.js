// components/aa/aa.js
// Component 构造器
Component({
  /**
   * 组件的属性列表
   */
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持  // 具名插槽当中可以开启
  },
  properties: {
    title:{
      type:"String",
      value:"默认值"
    }
  },
  lifetimes:{
    created(){
      console.log(this.properties.title)
      this.setData({
        title:123
      })
      console.log(this.properties.title)
    },
    // 相当于vuecreated
    attached(){
      this.setData({
        title:666
      })
    },
    detached(){
      console.log("被销毁了")
    },
  },
  //  created钩子函数当中不做什么操作
  
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    upt(){
      console.log(this.properties.title);
      // 在小程序当中默认也是不能修改组件外面的数据 单项数据流原则
      // 在组件中传递自定义事件
      // this.setData({
      //   title:3
      // })
      // 在组件当中修改页面对应的数据 
      this.triggerEvent("childEvents",{num:123})
    }
  }
})
