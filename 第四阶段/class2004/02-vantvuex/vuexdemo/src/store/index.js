import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    num:1,
    list:[ // this.$store.state.list
      {
        id:1,
        name:"苹果",
        price:10,
        num:3
      },
      {
        id:2,
        name:"香蕉",
        price:20,
        num:30
      },
      {
        id:3,
        name:"橘子",
        price:10,
        num:10
      },
    ],
    aboutList:[]
  },
  mutations: {
    // commit 提交
    add(state,id){ // this.$store.commit("add",1)
      // console.log(state.list)
    
      var item = state.list.find(ele =>ele.id == id);
      console.log(item);
      var obj = {
        id:item.id,
        name:item.name,
        price:item.price,
        num:1,
        isChecked:false
      };
      // if(item.num <= 0){
      //   return 
      // }
      // 添加之前先判断  aboutList 是否存在相同的商品
      var aboutItem  = state.aboutList.find(ele =>ele.id == id);
      if(aboutItem){
        aboutItem.num ++
      }else{
        state.aboutList.push(obj)
      }
      item.num -- 
     
      
    },
    // uptIsAllChecked 修改列表状态
    uptIsAllChecked(state,val){
      // state.aboutList;
      state.aboutList.forEach(ele =>ele.isChecked = val)
      // if(val){
      //   state.aboutList.forEach(ele =>ele.isChecked = val)
      // }else{
      //   state.aboutList.forEach(ele =>ele.isChecked = val)
      // }
    }
  },
  // getters 类似于前面的computed
  getters:{
    total(state){ // this.$store.getters.total
      return state.aboutList.reduce((total,current)=>{
        if(current.isChecked){
          return total + current.price*current.num
        }else{
          return total
        }
        
      },0)
    },
    isAllChecked(state){ // true false
      if(state.aboutList.length == 0){
        return false
      }
      return state.aboutList.every(ele =>ele.isChecked)
    }
  },
  actions: {
    // actions  类似于前面的methods
    syncAdd(context,id){ // context 上下文对象 Store实例 this.$store.dispatch("syncAdd")  // dispatch 分发
    // id 载荷 一次只能传递一个参数  传递多个参数 需要转换成数组或对象的方式
      console.log(context.state.num);
      // 不推荐这种方式 数据的变化并没有反应到调试工具当中
      context.state.num ++ ;
      // setTimeout(()=>{
      //   context.commit("add",id)
      // },1000)
    }
  },
  // modules 模块
  modules: {
  }
})
