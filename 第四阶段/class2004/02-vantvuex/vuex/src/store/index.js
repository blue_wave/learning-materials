import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({// Store 全局数据仓库
  state: {
    num:1 // 再任何组件都能访问到num this.$store.state.num
  },
  mutations: {
    // 类似于methods
    // 一切修改state 里面数据的方法都应该统一的写在mutations
    // 2/3/4 载荷  一次传递的载荷只能传递一个 如果需要传递多个参数 可以把参数合并成对象或者数组的方式
    // [a,b,c]=[1,2,3] 
    inc(state,[a,b]){ // this.$store.commit("inc",[2,"小明"])
      console.log(a,b)
      state.num += a ;
    }
  },
  actions: {
  },
  modules: {
  }
})
