
## CSS3

CSS3是CSS（层叠样式表）技术的升级版本，于1999年开始制订，2001年5月23日W3C完成了CSS3的工作草案，主要包括盒子模型、列表模块、超链接方式、语言模块、背景和边框、文字特效、多栏布局等模块。


## 浏览器内核以及其前缀

CSS标准中各个属性都要经历从草案到推荐的过程，css3中的属性进展都不一样，浏览器厂商在标准尚未明确情况下提前支持会有风险，浏览器厂商对新属性的支持情况也不同，所以会加厂商前缀加以区分。如果某个属性已经从草案变为了或接近推荐方案，并且厂商已经完全实现了推荐属性，那就不用加厂商前缀。如border-radius已经很成熟，不用加前缀。


## 什么是浏览器内核

所谓浏览器内核就是指浏览器最重要或者说核心的部分"Rendering Engine"，译为"渲染引擎"。负责对网页语法的解析，比如HTML、JavaScript，并渲染到网页上。所以浏览器内核也就是浏览器所采用的渲染引擎，渲染引擎决定这浏览器如何显示页面的内容和页面的格式信息。不同的浏览器内核对语法的解释也不相同，因此同一的网页在不同内核的浏览器显示的效果也会有差异。这也就是网页编写者在不同内核的浏览器中测试网页显示效果的原因。


根据不同的浏览器内核，css前缀会有不同。最基本的浏览器内核有如下四种，其它的内核都是基于此四种进行再研发的。

1. Gecko内核      前缀为-moz-   火狐浏览器

2. Webkit内核    前缀为-webkit-   也叫谷歌内核，chrome浏览器最先开发使用，safari浏览器也使用该内核。国内很多浏览器也使用了webkit内核，如360极速、世界之窗、猎豹等。

3 .Trident内核    前缀为-ms-  也称IE内核

4. Presto内核      前缀-o-   目前只有opera采用 

在大多数情况下，当你需要使用CSS3规范中的属性且需要使用一个前缀的时候，针对所使用的浏览器，添加上面的前缀就可以了。例如：如果想要添加一个CSS过渡效果，使用transition属性，并且先添加如下前缀:

```
-webkit-transition:background 0.5s ease;
-moz-transition:background 0.5s ease;
-o-transition:background 0.5s ease;
transition:background 0.5s ease;
```

用户的浏览器将会对它所理解的过渡功能版本做出响应，而忽略其他的版本。目前浏览器厂商对于全面实现所有的CSS3功能竭尽全力，并且对于大多数现代浏览器来说，需要添加前缀的属性的数目正在快速减少。

对于那些功能需要添加前缀，可以浏览: http://shouldiprefix.com/
处理添加前缀可以使用插件(autoprefix),有时候需要在css中进行编写

## CSS3新增

### 选择器

CSS3新增了许多灵活查找元素的方法，极大的提高了查找元素的效率和精准度。CSS3选择器与jQuery中所提供的绝大部分选择器兼容。

**子代选择器**

`div > p` 选择 `div`的子元素`p`标签(必须是亲儿子,不隔代)

**紧邻兄弟选择器**

`+`  选择下一个(紧挨着的)符合条件的兄弟元素

**通用兄弟选择器**

`~` 选择后边所有符合条件的兄弟元素

**属性选择器** 

通过属性来选择元素

`E[attr]`	选择包含attr属性的所有元素
`E[attr=mydemo]`	选择属性attr的值等于mydemo字符的所有元素
`E[attr*=mydemo]`	选择属性attr的值任意位置包含mydemo字符的所有元素
`E[attr^=mydemo]`	选择属性attr的值开始位置包含mydemo字符的所有元素
`E[attr$=mydemo]`	选择属性attr的值结束位置包含mydemo字符的所有元素

示例:

```html
<style>
	*{
		margin: 0;
		padding: 0;
	}
	p[title]{
		color: red;
	}
	p[title="hello"]{
		color: green;
	}
	p[class*=e]{
		color: orange;
	}
	p[class$=o]{
		color: #0000FF;
	}
	p[class^=w]{
		color: purple;
	}
	p[data-name]{
		font-size: 25px;
	}
</style>

<body>
	<p title="hello">今天天气真好啊</p>
	<p title="nice">今天天气真好啊</p>
	<p class="word">今天天气真好啊</p>
	<p class="text">今天天气真好啊</p>
	<p class="texa">今天天气真好啊</p>
	<p class="ao">今天天气真好啊</p>
	<p data-name="today">今天天气真好啊</p>
</body>
```

**伪类选择器**

除了以前学过的
`:link` 链接未被访问时
`:active` 链接点击时(一瞬间)
`:visited` 链接访问后
`:hover` 鼠标悬浮时

CSS3又新增了其它的伪类选择器

**表单相关伪类**

`:checked`	匹配被选中的选项
`:focus`	匹配获焦的输入框

**结构伪类** 

重点通过E来确定元素的父元素。
`E:first-child`	第一个子元素
`E:last-child`	最后一个子元素
`E:nth-child(n)` 第n个子元素
`E:nth-last-child(n)` 同`E:nth-child(n)` 相似，只是倒着计算
*注意*

1. n是支持简单表达式的
2. n 是从1开始的自然数 E:nth-child(0) 无效
3. 最好子元素是同样的元素

**目标伪类**

`E:target` 结合锚点进行使用，处于当前锚点的元素会被选中

```html
  <style>
  	li:target{
  		font-size: 30px;
  		color: blue;
  	}
  <style>
  <body>
  	<a href="#li3">点我</a>
  	<li id="li3">li3</li>
  </body>
```

**伪元素**
`E::before`、`E::after` 默认行内元素 content 属性必须写
`E::first-letter`文本的第一个字母或字
`E::first-line` 文本第一行
`E::selection` 被选中的文本

**":" 与 "::" 用于区分伪类和伪元素**


### 新增样式属性

#### Web字体

通过 `@font-face`属性,可以引入外部字体文件

```css
@font-face {
    font-family:"bf";
    src: url(bf.TTF); 
}
```

**字体图标**

将图标制作成字体包, 这样我们就可以将图标当做一个字来对待,对齐设置大小和颜色,实现图标的变色.
[矢量图标库](https://www.iconfont.cn/)

#### 文本阴影

`text-shadow: h-shadow(x) v-shadow(y) blur(模糊半径) color(颜色)`

1、水平偏移量 正值向右 负值向左

2、垂直偏移量 正值向下 负值向上

3、模糊半径是不能为负值

4、可以有多个影子，用逗号隔开

5、案例:浮雕文字

凹: `text-shadow: -1px -1px 1px #000, 1px 1px 1px #fff;`

凸: `text-shadow: -2px -2px 1px #fff, 2px 2px 1px #000;`

文字模糊: `text-shadow:0 0 100px rgba(0,0,0,0.5);`

文字描边: `-webkit-text-stroke: 4px pink;`

#### 盒子阴影

`box-shadow: h-shadow(x) v-shadow(y) blur(模糊半径) spread(扩展范围) color(颜色) inset(是否内嵌,可省略)`

[盒子阴影生成器](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Background_and_Borders/Box-shadow_generator)

#### 倒影

`-webkit-box-reflect` 生成倒影,支持 `above` `below` `right` `left`

添加间距:  `-webkit-box-reflect: below 10px;`

添加渐变:  `-webkit-box-reflect: below 0 linear-gradient(transparent, white);`

#### CSS3盒子模型(怪异盒子模型)

CSS3中可以通过box-sizing来指定盒模型，即可指定为content-box、border-box，这样我们计算盒子大小的方式就发生了改变

`content-box`: 对象的实际宽度等于设置的 width 值和 border、padding 之和 (默认方式)

`border-box`： 对象的实际宽度就等于设置的 width 值，即使定义有 border 和 padding 也不会改变对象的实际宽度，即 ( Element width = width ), 我们把这种方式叫做 css3 盒模型

作业练习: [日历]([https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=02003390_25_hao_pg&wd=%E6%97%A5%E5%8E%86&oq=%25E9%2598%25BF%25E9%2587%258C%25E5%25B7%25B4%25E5%25B7%25B4%25E7%259F%25A2%25E9%2587%258F%25E5%259B%25BE%25E6%25A0%2587&rsv_pq=a7759dc2000002ce&rsv_t=01001tDErsqkc4d0cOlzONbVSxUIhGisoS4F%2F7NQiSNGqR5W034GHmiPmsnTcEF%2FJ%2BEWqKTwDsqo&rqlang=cn&rsv_enter=1&rsv_dl=tb&inputT=1932&rsv_sug3=94&rsv_sug1=70&rsv_sug7=101&rsv_sug2=0&rsv_sug4=2527](https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=02003390_25_hao_pg&wd=日历&oq=%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4%E7%9F%A2%E9%87%8F%E5%9B%BE%E6%A0%87&rsv_pq=a7759dc2000002ce&rsv_t=01001tDErsqkc4d0cOlzONbVSxUIhGisoS4F%2F7NQiSNGqR5W034GHmiPmsnTcEF%2FJ%2BEWqKTwDsqo&rqlang=cn&rsv_enter=1&rsv_dl=tb&inputT=1932&rsv_sug3=94&rsv_sug1=70&rsv_sug7=101&rsv_sug2=0&rsv_sug4=2527)) (只有里边那一部分)



## 渐变

**线性渐变 沿着某条直线朝一个方向产生渐变效果**

语法: background: linear-gradient(direction, color-stop1, color-stop2, ...);

`direction`: 方向 可以是 角度(10deg)顺时针  也可以是 to top/left/right/bottom

eg: `background: linear-gradient(to right ,red 60%,orange 80%);`
颜色后面可以跟百分比,表示从百分几开始渐变

渐变的兼容写法 方向是相反的  而且不带to

eg:` background: -webkit-linear-gradient(right ,red 60%,orange 80%);`

**重复渐变: ** `background: repeating-linear-gradient(to right,red 0, red 10%, purple 10%, purple 20%)`

可简写为: `background: repeating-linear-gradient(to right,red 0 10%, purple 10% 20%)`

**一般是通过ui设计稿 直接提取的渐变 角度 颜色比重**

**径向渐变 从一个中心点开始沿着四周产生渐变效果**

语法: `background: radial-gradient((shape? size? (at position?))?, start-color, ..., last-color)` ? 表示可省略

`shape` 渐变形状 : circle | ellipse(椭圆)
`size` 渐变大小:
	`closest-side`（指定径向渐变的半径长度为从圆心到离圆心最近的边）
	`closest-corner` （指定径向渐变的半径长度为从圆心到离圆心最近的角）
	`farthest-side`（指定径向渐变的半径长度为从圆心到离圆心最远的边）
	`farthest-corner`（指定径向渐变的半径长度为从圆心到离圆心最远的角）
也可指定大小: 需要注意的是 若渐变形状为圆形，则该渐变大小只能设置一个数且不能为百分数，而椭圆既可以为具体数值也可以为百分数

注意: 只传一个值默认形状为圆形,传入的是半径大小,不能为百分比;

传两个值则代表形状为椭圆形,第一个是x轴半径,第二个是y轴半径

圆心位置参数一定要置于radial-gradient()第一个参数的末尾，顺序千万不能放反了
	
**重复渐变** : `background: repeating-radial-gradient(circle at center,#f00 0,#f00 10%,#ff0 10%,#ff0 20%);`
可简写为:` background: repeating-radial-gradient(circle at center,#f00 0 10%,#ff0 10% 20%);`

## 背景图片增强

1. `background-size`设置背景图片的尺寸
	`cover` 会自动调整缩放比例，保证图片始终填充满背景区域，如有溢出部分则会被隐藏
	`contain` 会自动调整缩放比例，保证图片始终完整显示在背景区域
	注意：是需要根据高度还是宽度适配 width height 直接设置宽高(px值或者百分比)
2. `background-origin`(原点，起点)设置背景定位的原点
	`border-box` 以边框做为参考原点；
	`padding-box` 以内边距做为参考原点；
	`content-box` 以内容区做为参考点；
3. `background-clip`设置背景区域 clip(裁切)
	`border-box` 裁切边框以内为背景区域；
	`padding-box` 裁切内边距以内为背景区域；
	`content-box` 裁切内容区做为背景区域
4. 以逗号分隔可以设置多背景，可用于自适应局
	`background: url("img/1.jpg") no-repeat left top, url(img/2.jpg)` no-repeat right top;



## 过渡

`transition` 过渡 实现元素不同状态间的平滑过渡，经常用来制作动画效果

在 CSS3 的世界里，让网页元素动起来的第一个方法是利用 transition，基于 transition 可以让元素的某个 CSS 属性从指定的开始状态过渡到特定的结束状态。我们将元素「从指定的开始状态过渡到特定的结束状态」这个过程简称为「状态变换」，注意这里的过渡，事实上 transition 便像是页面元素「状态变换」的润滑剂，如果没有 transition，元素「状态变换」的过程将会显得生硬而突兀（如下图中左边的小圆球，[查看 DEMO](https://codepen.io/mamboer/full/pLvLyv/)）。

![transition](https://user-gold-cdn.xitu.io/2018/3/10/1620f2835b9b7448?w=950&h=220&f=gif&s=97142)

transition 可作用于普通的 CSS 属性（如 background 、opacity ...），也可以作用于 CSS3 出现时新引入的 transform 属性，而利用后者可以实现 3D 的过渡效果。transform 属性就像是 CSS3 这个动效武器子弹里的火药，大家可以通过 MDN 的 《transform》 一文进行进一步地了解学习，务必做到深谙其门道，避免一知半解。

`transition: transition-property transition-duration transition-timing-function transition-delay;`

`transition-property`   规定应用过渡的 CSS 属性的名称 (一般都写 all);

`transition-duration`   定义过渡效果花费的时间。默认是 0

`transition-timing-function`  规定过渡效果的时间曲线。默认是 "ease"。

linear|ease|ease-in|ease-out|ease-in-out|cubic-bezier(n,n,n,n);

`transition-delay` 规定过渡效果何时开始。默认是 0

`transform` 用于设置转换动画 如旋转、位移、倾斜等

```
transform: scale? translate? rotate? skew?;

缩放 scale(x, y) 可以对元素进行水平和垂直方向的缩放，x、y的取值可为小数，不可为负值，设置一个值时表示 x、y同时进行缩放

移动 translate(x, y) 可以改变元素的位置，x、y可为负值

旋转 rotate(deg) 可以对元素进行旋转，正值为顺时针，负值为逆时针

倾斜 skew(x-angle,y-angle)	定义沿着 X 和 Y 轴的 2D 倾斜转换
		
skewX(x-angle) 	定义沿着 X 轴的 2D 倾斜转换
		
skewY(y-angle)	定义沿着 Y 轴的 2D 倾斜转换
```

## Transition 动画的局限性和适用性

transition 实现的动画有下面这些特点：

1. 支持有限的 CSS 属性
   可通过[《CSS animated properties》](https://developer.mozilla.org/zh-CN/docs/Web/CSS/CSS_animated_properties)一文查看支持过渡动效的 CSS 属性。

2. 隐式过渡（implicit transitions）
   transition 的过渡动画是隐式的（如下图所示，图片来源于 MDN），即除了动画的开始状态和结束状态我们可以自定义之外，「状态变换」的具体过程由浏览器自动执行，中途无法进行人为干预。

![隐式过渡](https://user-gold-cdn.xitu.io/2018/3/12/1621877fdfc0d719?w=706&h=204&f=png&s=18685)

3. 一次性、不可暂停或反转
   transition 只支持两个状态之间的变换过渡，不支持多个状态的连续变换过渡，并且状态的变换是一次性的（无法循环）， 不可暂停，且不可反转（从状态 A 过渡到 B 后不能立即又过渡回 A）。

所以，在实际应用中我们常常利用 transition 来做那些轻量的、修饰性的动效，用于增强用户在网页上操作时得到的反馈。例如：

- 元素「hover」 或「点击」后的反馈
- 弹窗「打开」或「关闭」时的效果
- ...

## 3D变换

3D 变换相较 2D 变换，坐标系中多了 Z 轴，也就意味着物体除了上下左右，还可以前后移动。

```css
rotateX/Y/Z 	沿X/Y/Z轴旋转

translateX/Y/Z 	沿X/Y/Z轴移动

rotate() 在 2D 中的旋转方式，在 3D 中与 rotateZ 相当。
```

那么，单纯地将 `transform` 中的参数扩展出 Z 维度，就能实现 3D 效果了吗？

![示例图片](https://user-gold-cdn.xitu.io/2018/2/22/161be021b5fca407)

#### 透视

什么是 `perspective` ？词典中翻译为观点、远景、透视图。这是一个非常抽象的概念，需要一点空间想象力。

在浏览器中边调整 `perspective` 数值边观察 3D 效果。

![3D效果](https://user-gold-cdn.xitu.io/2018/2/22/161be041596ded52?w=375&h=250&f=gif&s=141903)

*透视会产生近大远小的效果*

`perspective`有两种写法

a) 作为一个属性，设置给父元素，作用于所有3D转换的子元素

b) 作为 transform 属性的一个值，作用于元素自身,子元素的3D效果可呈现

![消失点](https://user-gold-cdn.xitu.io/2018/2/22/161be04955ddd5e2)

左图与右图的元素均绕 Y 轴旋转了 45°，但差别很明显，右图更容易让人想到一个画面中集体开启的窗户。左图的问题就在于，每个元素的消失点各自为政，都在元素的中心点位置，而右图的消失点则统一在实线方框的中心位置。实现方法就是将元素的 `perspective` 设置转移至元素父容器上。

#### 建立三维空间体系

![三维空间体系](https://user-gold-cdn.xitu.io/2018/2/22/161be073a53fcfca?w=750&h=440&f=png&s=55534)

`transform-style: preserve-3d` 的属性设置，默认值为 `flat`。这个属性的设置旨在告诉子元素需要遵循怎样的空间体系规则。这个属性不能继承，因此只要有子元素需要设置空间体系规则，就得在父元素声明这个属性。

`backface-visibility：visible/ hidden` 设置元素背面是否可见

#### 案例

结合透视和3D转换画一个立方体:

- 步骤 1 - 准备立方体的 HTML 代码
- 步骤 2 - 利用 CSS(3) 将 6 个面组装成立体形状的立方体
- 步骤 3 - 让立方体显得更立体点

```
为了让立方体默认看起来更立体点（不是单纯地正面对着我们），可以利用 `rotate` 将立方体在 X 和 Y 轴上各旋转 `25deg`，让它正面斜对着我们。
```

- 步骤 4 - 设置立方体的 `transition` 属性

```
最后一步就是给立方体添加 `transition` 属性，让它的状态变换拥有过渡动画效果
```

- 步骤 5 - 在立方体的父级元素上设置透视距离：`perspective: 1000px`，在立方体上设置变形方式：`transform-style: preserve-3d`

```html
<style>
	*{
		margin: 0;
		padding: 0;
	}
	.box{
		position: relative;
		width: 200px;
		height: 200px;
		margin: 50px auto;
		border: 1px dashed red;
		transform-style: preserve-3d;
		transition: all 2s linear;
		transform: rotateX(25deg) rotateY(25deg);
	}
	
	.box div{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	
	.font{
		transform: translateZ(100px);
		background-color: blue;
	}
	.behand{
		transform: translateZ(-100px);
		background-color: pink;
	}
	.left{
		transform: translateX(-100px) rotateY(90deg);
		background-color: red;
	}
	.right{
		transform: translateX(100px) rotateY(90deg);
		background-color: brown;
	}
	.top{
		transform: translateY(-100px) rotateX(90deg);
		background-color: yellow;
	}
	.bottom {
		transform: translateY(100px) rotateX(90deg);
		background-color: purple;
	}
	
	.box:hover{
		transform: rotateX(360deg) rotateY(360deg);
	}
</style>
<body>
	<div class="box">
		<div class="font"></div>
		<div class="behand"></div>
		<div class="left"></div>
		<div class="right"></div>
		<div class="top"></div>
		<div class="bottom"></div>
	</div>
</body>
```

翻书效果:

一本合上的书正常来说是在 Y 轴右侧，每一页都包含两面，也就是说一本书是由若干个翻页效果组合而成，每一页的变换原点`transform-origin`在元素左侧。由此可以在翻牌的基础上迅速整出一个翻书 DEMO（猛戳 [查看翻书 DEMO](http://lyxuncle.github.io/pageturning/demo/demo.html)）。

#### 终端支持

由于截至目前为止，CSS3 的 3D 功能还止于炫技的阶段，安卓机与 iOS 的支持效果存在差异且难以调和，不建议在生产项目中大面积使用 3D 深层功能。

![买家秀](https://user-gold-cdn.xitu.io/2018/2/22/161be0c7c0d7c03d?w=750&h=320&f=png&s=30011)

## 3D 与硬件加速

坊间流传这这样一个传说：一旦使用 3D 属性，就能触发设备的硬件加速，从而使得浏览器的表现更佳。但这句话也得看情境——

> 想象使用 GPU 加速的动画就像是 Vin Diesel（速度与激情的主角）开着 Dominic 标志性的汽车 —— Dodge Charger。它的定制 900 hp 引擎可以让它在一瞬间从 0 加速到 60 码。但是如果你开着它在拥挤的高速公路上又有什么用呢？这种情况下错的不是你的车辆，而是你还在一条拥堵的高速公路上。—— [《CSS 硬件加速的好与坏》](http://efe.baidu.com/blog/hardware-accelerated-css-the-nice-vs-the-naughty/)

因此千万别贪心，将 3D 效果数量控制在一定范围内，页面性能才是重中之重。