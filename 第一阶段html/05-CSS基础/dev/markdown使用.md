# 这是一个标题

## 二级标题

### 三级标题

#### 四级标题

##### 五级标题

###### 六级标题


## 段落

这是一个段落街坊邻居看看就看就看就看就看

这是一个段落街坊邻居看看就看就看就看就看



## 图片怎么引入

![sorry,图片未找到](https://pics5.baidu.com/feed/0b7b02087bf40ad17b9d9da357a78dd9a8ecce82.jpeg?token=ad5ca5b16ad7738240a9ba474f65d6a6)



## 链接如何使用

[百度](https://www.baidu.com/)



## 代码块如何引入

```javascript
var a = 10;

```

```css
.text{
	color:red;
}
```

```html
<div class="zx">
	<div class="zx-item qq">
		<ul class="zx-details">
			<li class="title">
				在线咨询
				<span class="close"></span>
			</li>
			<li class="teacher">
				王老师
			</li>
			<li class="teacher">
				张老师
			</li>
			<li class="tel">
				<div class="tel-item">
					联系电话
				</div>
				<div class="tel-number">
					0371-666666
				</div>
			</li>
		</ul>
	</div>
	<div class="zx-item wx">
		<div class="wx-qr"></div>
	</div>
	<div class="zx-item go-top"></div>
</div>

```

## 表格

| 属性     |       值 | Qty |
| :------- | -------: | :-: |
| Computer | 1600 USD |  5  |
| Phone    |   12 USD | 12  |
| Pipe     |    1 USD | 234 |


## 列表

### 有序列表

1. 项目1
2. 项目2
3. 项目3


### 无序列表

- 列表项1
- 列表项2
- 列表项3

+ 列表项1
+ 列表项2
+ 列表项3

* 列表项1
* 列表项2
* 劣币下3

### 列表嵌套
1. 项目1
	- 列表1
	- 列表2
	- 列表3

2. 项目2
	- 列表1
	- 列表2

_斜体_
**加粗**
~~删除线~~

> 这会一段引用的话,比如 名人名言,比如说一本书的 序,前言



---



### 行内代码

`我是一句很短的代码`

`var b = 1000;`