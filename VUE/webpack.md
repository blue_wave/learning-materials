# webpack 概述

> webpack 是一个现代 javascript 应用程序的 **静态模块打包器 (module bundler)**

[webpack官网](https://webpack.js.org/)

gulp和grunt是流管理工具，通过一个个task配置执行用户需要的功能

grunt配置复杂繁重，是基于文件流的操作，比较慢；gulp是基于内存流的操作，配置轻量级，代码组织简单易懂，异步任务。

webpack和browserify是前端模块化方案，与seajs和requirejs是一个东西，只不过seajs和requirejs是在线编译方案，引入一个CMD\AMD编译器，让浏览器能认识export、module、define等，而webpack和browserify是预编译方案，提前将es6模块、AMD、CMD模块编译成浏览器认识的js。

## webpack 能做什么

webpack是一个静态模块打包器

1. 语法转换
   + less/sass/stylus转换成css
   + ES6转换成ES5
   + ...
2. html/css/js 代码压缩合并 (打包)
3. webpack可以在开发期间提供一个开发环境
   + 自动打开浏览器
   + 保存时自动刷新
4. 项目一般先打包再上线

# webpack 的基本使用

## webpack基本打包配置

1. 建目录  dist    src/main.js

2. 初始化

   ```
   npm init -y
   ```

3. 安装依赖包 (-D 将依赖记录成开发依赖, 只是开发中需要用的依赖, 实际上线不需要的)

   ```
   npm add webpack  webpack-cli  -D
   ```

4. 到package.json文件中, 配置scripts 

   ![image-20200705173010546](C:\Users\董倩\AppData\Roaming\Typora\typora-user-images\image-20200705173010546.png)

   ```js
   scripts: {
   	"build": "webpack --config webpack.config.js"
   }
   ```

5. 提供 webpack.config.js 

   参考文档:   [https://webpack.docschina.org/concepts/#入口-entry-](https://webpack.docschina.org/concepts/#入口-entry-) 

   ```js
   const path = require('path')
   
   module.exports = {
     // entry: 配置入口文件 (从哪个文件开始打包)
     entry: './src/main.js',
   
     // output: 配置输出 (打包到哪去)
     output: {
       // 打包输出的目录 (必须是绝对路径)
       path: path.join(__dirname, 'dist'),
       // 打包生成的文件名
       filename: 'bundle.js'
     },
   
     // 打包模式 production 压缩/development 不压缩
     mode: 'development'
   }
   ```

6. 执行配置的scripts脚本

   ```
   yarn build
   ```

   小测试:

   ​	假定在main.js中导入一个  aa.js,  多个文件需要打包, wepack会打包成一个文件, 可以节约请求的次数

   ```js
   require('./aa.js')
   console.log('这是main模块')
   ```

   ## 基于 webpack 实现样式

   - 新建  public/index.html 编写代码

   - 在 index.html 中新建一些 li 玩玩

     ```html
     
     ```
   
   需求:
   
   **使用 jquery 隔行变色**
   
   安装jquery
   
   ```
   yarn add jquery
   ```
   
   `main.js`
   
   ```js
   // 需求: 通过jquery实现隔行变色
   const $ = require('jquery')
   $(function() {
     $('#app li:nth-child(odd)').css('color', 'red')
     $('#app li:nth-child(even)').css('color', 'green')
   })
   ```
   
   ## **自动生成html** - html-webpack-plugin插件
   
   在 index.html 中 手动引入 打包后的资源，是有缺点的
   
   比如: `如果webpack 配置中的输出文件名修改了，需要及时在 index.html 中同步修改`

     1. 下载 (-D 将依赖记录成开发依赖, 只在开发阶段用, 实际上线是不需要的)

        ```
     yarn add html-webpack-plugin  -D
        ```

     2. **在`webpack.config.js`文件中，引入这个模块** :
   
        ```js
     // 引入自动生成 html 的插件
        const HtmlWebpackPlugin = require('html-webpack-plugin')
     ```
   
     3. 配置
   
        ```js
        plugins: [
          new HtmlWebpackPlugin({ template: './public/index.html' })
        ]
        ```

   > 配置好了之后, public 目录的 index.html 就不需要引入打包后的文件了, 会自动被插件生成 html 引入

   # webpack - loaders 的配置

webpack默认只认识 js 文件, 但是webpack 可以使用 [loader](https://www.webpackjs.com/concepts/loaders) 来加载预处理文件, 允许webpack也可以打包 js之外的静态资源。

所以webpack如果要处理其他文件类型, **记得要先配置对应的 loader**

## webpack中处理 css 文件

需求: 去掉小圆点, 新建 css 目录

1. 安装依赖

   ```
   yarn add style-loader css-loader -D
   ```

2. 配置

   ```js
   module: {
     // loader的规则
     rules: [
       {
         // 正则表达式，用于匹配所有的css文件
         test: /\.css$/,
         // 先用 css-loader 让webpack能够识别 css 文件的内容
         // 再用 style-loader 将样式, 以动态创建style标签的方式添加到页面中去
         use: [ "style-loader", "css-loader"]
       }
     ]
   },
   ```

##  分离 css 文件

将css放到了style标签中, 请求次数是少了, 

但是如果css文件太大的话，也不是太好，那有没有什么办法把`css`分离出来呢？ 

- 有一个插件，`mini-css-extract-plugin`，这个插件支持`webpack4.x`

- 之前的插件`extract-text-webpack-plugin`对`webpack3.x`的版本支持 (目前已废弃)

1. 安装依赖包

   ```
   yarn add mini-css-extract-plugin -D
   ```

2. **在`webpack.config.js`文件中，引入这个模块** 

   ```js
   // 引入分离 css 文件的 模块
   const MiniCssExtractPlugin = require('mini-css-extract-plugin')
   ```

3. 配置loaders

   使用这个loader也很简单，只有将style-loader 替换成 MiniCssExtractPlugin.loader, ‘css-loader 

   我们需要首先在js 中引用相应的css文件

   require('../css/index.css');

   ```js
   // 模块加载
   module: {
     // loader的规则
     rules: [
       // 配置 css 文件的解析
       {
         test: /\.css$/,
         use: [ // 根据官方文档写的，注意'css-loader'的书写位置
           {
             loader: MiniCssExtractPlugin.loader,
             // 这里可以指定一个 publicPath
             // 默认使用 webpackOptions.output中的publicPath
            // publicPath的配置，和plugins中设置的filename和chunkFilename的名字有关
           // 如果打包后，background属性中的图片显示不出来，请检查publicPath的配置是否有误
             options: {
               publicPath:'../',
             },
           },
           'css-loader'
         ]
       },
     ],
   }
   ```

4. 插件的配置

   ```js
   // 配置插件
   plugins: [
   	...
     // 定义打包好的文件的存放路径和文件名
       // 这里的配置和webpackOptions.output中的配置相似
         // 即可以通过在名字前加路径，来决定打包后的文件存在的路径
     new MiniCssExtractPlugin({ 
       filename: "css/[name].[hash].css",
       chunkFilename: 'css/[id].[hash].css',
    })
   ],
   ```


