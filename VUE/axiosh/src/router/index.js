import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
	// {
	//   path: '/',
	//   name: 'Home',
	//   component: Home
	// },
	// {
	//   path: '/about',
	//   name: 'About',
	//   // route level code-splitting
	//   // this generates a separate chunk (about.[hash].js) for this route
	//   // which is lazy-loaded when the route is visited.
	//   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
	// },

	{
		path: '/post1',
		name: 'Post',
		component: () => import('../components/Post.vue')
	},
	{
		path: '/baseurl',
		name: 'Baseurl',
		component: () => import('../components/Baseurl.vue')
	},
	{
		path: '/login',
		name: 'Login',
		component: () => import('../components/Login.vue')
	},
	{
		path: '/adduser',
		name: 'Adduser',
		component: () => import('../components/Adduser.vue')
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
