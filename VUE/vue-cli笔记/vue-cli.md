**一、介绍**

Vue CLI 是一个基于 Vue.js 进行快速开发的完整系统。有三个组件：

**CLI**：`@vue/cli` 全局安装的 npm 包，提供了终端里的vue命令（如：vue create 、vue serve 、vue ui 等命令）

**CLI 服务**：`@vue/cli-service`是一个开发环境依赖。构建于 webpack 和 webpack-dev-server 之上（提供 如：`serve`、`build` 和 `inspect` 命令）

**CLI 插件**：给Vue 项目提供可选功能的 npm 包 （如： Babel/TypeScript 转译、ESLint 集成、unit和 e2e测试 等）

**二、安装**

1、全局安装过旧版本的 `vue-cli`(1.x 或 2.x)要先卸载它，否则跳过此步：

```
npm uninstall vue-cli -g //或者 yarn global remove vue-cli
```

　　

2、Vue CLI 3需要 nodeJs ≥ 8.9 (官方推荐 8.11.0+，你可以使用 nvm 或 nvm-windows在同一台电脑中管理多个 Node 版本）。

（2）下载安装nodeJs，**中文官方**下载地址：[http://nodejs.cn/download/ ](http://nodejs.cn/download/)

3、安装@vue/cli（Vue CLI 3的包名称由 `vue-cli` 改成了 `@vue/cli`）

```
 cnpm install -g @vue/cli //yarn global add @vue/cli
```

```
　vue -V   检查vue版本号
```

 

三、创建项目
执行：

```
vue create my-project
```

会弹出来

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\01.png)





此处有两个选择：

1.default (babel, eslint) 默认套餐，提供 babel 和 eslint 支持。
2.Manually select features 自己去选择需要的功能，提供更多的特性选择。比如如果想要支持 TypeScript ，就应该选择这一项。
可以使用上下方向键来切换选项。如果只需要 babel 和 eslint 支持，那么选择第一项，就完事了，静静等待 vue 初始化项目。

如果想要更多的支持，就选择第二项：切换到第二项，按下 enter 键选中，弹出如下界面：

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\02.png)



![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\03.png)



vue-cli 内置支持了8个功能特性，可以多选：使用方向键在特性选项之间切换，使用空格键选中当前特性，使用 a 键切换选择所有，使用 i 键翻转选项。

对于每一项的功能，此处做个简单描述：

```
TypeScript ：支持使用 TypeScript 书写源码
Progressive Web App (PWA) Support： PWA（让网页渐进式地变成App ） 支持
Router ：支持 vue-router 。
Vuex ：支持 vuex 。
CSS Pre-processors：支持 CSS 预处理器。
Linter / Formatter：支持代码风格检查和格式化。
Unit Testing ：支持单元测试。
E2E Testing ：支持 E2E 测试。
```

按下 enter 键确认选择，进入下一步：

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\04.png)



这里是让选择在开发 Vue 组件时，要不要使用 class 风格的写法。为了更方便地使用 TypeScript  ：

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\05.png)

```
? Please pick a preset: Manually select features
? Check the features needed for your project: Router, Vuex, CSS Pre-processors, Linter, Unit
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default):
> SCSS/SASS  //Sass安装需要Ruby环境，是在服务端处理的，SCSS 是 Sass3新语法（完全兼容 CSS3且继承Sass功能）
  LESS       //Less最终会通过编译处理输出css到浏览器，Less 既可以在客户端上运行，也可在服务端运行 (借助 Node.js)
  Stylus     //Stylus主要用来给Node项目进行CSS预处理支持，Stylus功能上更为强壮，和js联系更加紧密，可创建健壮的、动态的的CSS。
```



 ESLint：提供一个插件化的javascript代码检测工具

```
? Pick a linter / formatter config: (Use arrow keys)
> ESLint with error prevention only  //只有错误预防
  ESLint + Airbnb config   //Airbnb 配置
  ESLint + Standard config  //标准配置
  ESLint + Prettier         //使用较多  (漂亮的配置)
```

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\06.png)

何时检测：

```
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)
>( ) Lint on save                    // 保存就检测
 ( ) Lint and fix on commit          // fix和commit时候检查
```

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\07.png)

![](C:\Users\bufan\Desktop\vue-cli笔记\vue-cli图\08.png)

何时保存？



如何存放配置 

```
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? (Use arrow keys)
> In dedicated config files // 独立文件放置
  In package.json // 放package.json里
```

 ⑦  是否保存本次配置（之后可以直接使用）：

```
? Save this as a preset for future projects? (Y/n) // y:记录本次配置，然后需要你起个名; n：不记录本次配置
```

```
初始完之后，进入到项目根目录：

cd my-project
启动项目：

npm run serve
```

```
打包上线
在开发完项目之后，就应该打包上线了。 vue-cli 也提供了打包的命令，在项目根目录下执行：

npm run build
执行完之后，可以看到在项目根目录下多出了一个 dist 目录，


```

配置区别：

1）vuex（状态管理）：

 vue cli 2 中：vuex是搭建完成后自己npm install的，并不包括在搭建过程中。可以看到vue cli 2的vuex文件夹（store）又包含了三个js文件：action（存放一些调用外部API接口的异步执行的的方法，然后commit mutations改变mutations 数据）、index（初始化mutations 数据，是store的出口）、mutations（处理数据逻辑的同步执行的方法的集合，Vuex中store数据改变的唯一方法commit mutations）

vue cli 3 中：vuex是包含在搭建过程供选择的预设。vue cli 3 中的只用一个store.js代替了原来的store文件夹中的三个js文件

2）去掉 static 、 新增 public 文件夹

vue cli 2 ：static 是 webpack 默认存放静态资源的文件夹，打包时会直接复制一份到dist文件夹不会经过 webpack 编译 

vue cli 3 ：摒弃 static 新增了 public 。vue cli 3 中“静态资源”两种处理方式：

  经webpack 处理：在 JavaScript 被导入或在 template/CSS 中通过“相对路径”被引用的资源会被编译并压缩

  不经webpack 处理：放置在 public 目录下或通过绝对路径被引用的资源将会“直接被拷贝”一份，不做任何编译压缩处理

3） index.html ：

vue cli 2 ：“index.html ” 

vue cli 3 ：“public/index.html ”此模板会被 [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) 处理的

4） src/views：vue cli 3 的 src文件夹 新增 views文件夹 用来存放 “页面”，区分 components（组件）

5） 去掉 build（根据config中的配置来定义规则）、config（配置不同环境的参数）文件夹 ：

 vue cli 3 中 ，这些配置 你可以通过 命令行参数、或 `vue.config.js` （在根目录 新建一个 vue.config.js 同名文件）里的 [devServer](https://cli.vuejs.org/zh/config/#devserver) 字段配置开发服务器 

6） babel.config.js：配置Babel 。Vue CLI 使用了 Babel 7 中的新配置格式 `babel.config.js`。和 `.babelrc` 或 `package.json` 中的 `babel` 字段不同，这个配置文件不会使用基于文件位置的方案，而是会一致地运用到项目根目录以下的所有文件，包括 `node_modules` 内部的依赖。官方推荐在 Vue CLI 项目中始终使用 `babel.config.js` 取代其它格式。

7） 根目录的一些其他文件的改变：之前所有的配置文件都在vue create 搭建时preset预设 或者 后期可以通过 命令参数 、 `vue.config.js 中配置`

```
根据需要在根目录下新建 vue.config.js自行配置
```







**切换npm 镜像**



安装nrm

```
npm install -g nrm
```

查看npm镜像

```
nrm ls
可以看到有下列镜像
```

查看npm镜像

```
nrm ls
```


可以看到有下列镜像


可以看到有下列镜像

  # npm官方镜像


```
npm -------- https://registry.npmjs.org/
yarn ------- https://registry.yarnpkg.com/
```


  # cnpm镜像


```
 cnpm ------- http://r.cnpmjs.org/
```


  # 淘宝镜像,带*表示使用的是淘宝镜像
* ```
  taobao ----- https://registry.npm.taobao.org/
  nj --------- https://registry.nodejitsu.com/
  npmMirror -- https://skimdb.npmjs.com/registry/
edunpm ----- http://registry.enpmjs.org/
  ```
```

```
切换镜像
切换镜像只需要使用nrm use 镜像名称就可以切换镜像
```



# 切换为淘宝镜像
```
nrm use taobao
```


```