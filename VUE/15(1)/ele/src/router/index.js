import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
	  path:'/move',
	  name:'Move',
	  component:()=>import('../components/Move.vue')
  },
  {
  	  path:'/list',
  	  name:'List',
  	  component:()=>import('../components/List.vue')
  },
  {
	  path:'/echarts',
	  name:'Echarts',
	  component:()=>import('../components/Echarts')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
