(function (window) {
	'use strict';
	window.vm = new Vue({
		el:"#app",
		updated () {
			localStorage.setItem("list",JSON.stringify(this.list))
		},
		// vue实例创建的时候就会执行的生命周期钩子
		created () {
			if(localStorage.getItem("list")){
				this.list = JSON.parse(localStorage.getItem("list"))
			}
		},
		data () {
			return {
				name:"张三",
				ipt:"",
				current:"all",
				list:[
					{
						val:"吃饭",
						isChecked:true,
						isIptShow:false,
						id:Math.random()
					},
					{
						val:"睡觉",
						isChecked:true,
						isIptShow:false,
						id:Math.random()
					},
				]
			}
		},
		computed: {
			isCearShow(){
				return this.list.filter(ele => ele.isChecked == true).length == 0
			},
			lists(){ // 
				// 全部的数据
				if(this.current == "all" ){ // 全部的数据
					return this.list
				}else if(this.current == "active") { // 返回的没有勾选的数据 
					return this.list.filter(ele => ele.isChecked == false)
				}else{ // 返回的是 已经勾选的数据
					return this.list.filter(ele => ele.isChecked == true)
				}
				
				// 返回的没有勾选的数据 
				
				// 返回的是已经勾选的数据
				
			},
			nums(){
				var lists = this.list.filter(ele => ele.isChecked == false)
				return lists.length ;
				// return this.list.reduce((total,current)=>{

				// },0)
			},
			isAllChecked:{
				get(){ 
					return this.list.every(ele => ele.isChecked == true);
				},
				set(val){
					this.list.forEach(element => {
						element.isChecked = val
					});
				}
			}
		},
		directives: {
			focus:{
				// 当指令所绑定的元素插入到dom的时候所执行的钩子函数
				inserted(el){
					el.focus();
				}
			}
		},
		methods: {
			getVal(){

			},
			upt(val){
				var item = this.list.find(ele => ele.id == val);
				item.isIptShow = !item.isIptShow;
			},
			del(val){
				var index = this.list.findIndex(ele => ele.id == val);
				this.list.splice(index,1);
			},
			add(){
				if(this.ipt == ""){
					alert("输入框内容不能为空！")
					return 
				}
				var obj = {
					val:this.ipt,
					isChecked:false,
					isIptShow:false,
					id:Math.random()
				};
				this.list.push(obj);
				this.ipt = "";
			}
		}
	})

})(window);
